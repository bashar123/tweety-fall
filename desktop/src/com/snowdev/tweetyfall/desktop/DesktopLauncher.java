package com.snowdev.tweetyfall.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.snowdev.tweetyfall.TweetyFallGame;
import com.snowdev.tweetyfall.shared.Constants;

import static com.snowdev.tweetyfall.shared.Constants.HIDE_LWJGL_WINDOW_BORDER;
import static com.snowdev.tweetyfall.shared.Constants.LWJGL_APPLICATION_HEIGHT;
import static com.snowdev.tweetyfall.shared.Constants.LWJGL_APPLICATION_WIDTH;


public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.title = Constants.GAME_TITLE;
		config.width = LWJGL_APPLICATION_WIDTH;
		config.height = LWJGL_APPLICATION_HEIGHT;

		if(HIDE_LWJGL_WINDOW_BORDER)
			System.setProperty("org.lwjgl.opengl.Window.undecorated", "true");


		config.foregroundFPS = 60;
		new LwjglApplication(new TweetyFallGame(), config);
	}

}
