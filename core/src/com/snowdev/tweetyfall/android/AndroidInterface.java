package com.snowdev.tweetyfall.android;

public interface AndroidInterface {
     void screenOn();
     void screenOff();
     void showToast(String text);
}
