package com.snowdev.tweetyfall.screens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.snowdev.tweetyfall.TweetyFallGame;
import com.snowdev.tweetyfall.controllers.GameController;
import com.snowdev.tweetyfall.models.game_screen.GameFacade;
import com.snowdev.tweetyfall.screens.abstracts.ScreenClass;

import static com.snowdev.tweetyfall.shared.Constants.*;


public class GameScreen extends ScreenClass {

    private GameFacade facade;
    private Stage stage;

    public GameScreen(final TweetyFallGame game){
        super(game, true);
        Viewport vp = new StretchViewport(game.cam.viewportWidth, game.cam.viewportHeight, game.cam);
        stage = new Stage(vp);
    }


    @Override
    public void showAbstract(){
        facade = new GameFacade(game);
        Gdx.input.setInputProcessor(new GameController(facade));

        populateStages();
    }

    private void populateStages(){
        for(Actor actor : facade.getAllGameActors()){
            stage.addActor( actor );
        }
    }

    @Override
    public void render(float delta) {
        delta *= SLOWMO;

        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        facade.update(delta);

        renderStages(delta);

//        stage.setDebugAll(true);
        renderBox2dLights();
        if (DEBUG_RENDER)
            renderBox2dDebugger();

    }

    private void renderStages(float delta) {
        stage.act(delta);
        stage.draw();
    }

    private void renderBox2dLights(){
        if(BOX2D_LIGHTS){
            facade.getRayHandler().setCombinedMatrix(game.cam);
            facade.getRayHandler().updateAndRender();
        }
    }

    private void renderBox2dDebugger(){
            facade.getBox2DDebugRenderer().render(facade.getWorld(), game.cam.combined);
    }

    //todo: memory improvements
    @Override
    public void dispose() {
        System.out.println("GameScreen dispose");

        if(stage != null)
            stage.dispose();

        if(facade != null)
            facade.dispose();

    }
}
