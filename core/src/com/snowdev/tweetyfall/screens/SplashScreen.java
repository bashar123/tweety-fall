package com.snowdev.tweetyfall.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.snowdev.tweetyfall.TweetyFallGame;
import com.snowdev.tweetyfall.models.shared.extensions.MyImage;
import com.snowdev.tweetyfall.models.loading_screen.AssetsHandler;
import com.snowdev.tweetyfall.models.shared.extensions.MyStage;
import com.snowdev.tweetyfall.screens.abstracts.ScreenClass;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.rotateBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;
import static com.snowdev.tweetyfall.shared.Constants.CAMERA_HEIGHT_METER;
import static com.snowdev.tweetyfall.shared.Constants.CAMERA_WIDTH_METER;
import static com.snowdev.tweetyfall.shared.Constants.PositionXE.CENTER_X;
import static com.snowdev.tweetyfall.shared.Constants.PositionYE.VALUE_Y;
import static com.snowdev.tweetyfall.shared.Constants.SLOWMO;
import static com.snowdev.tweetyfall.shared.Constants.ScreenE.MENU_SCREEN;
import static com.snowdev.tweetyfall.shared.Constants.LOADING_BAR_COLOR;
import static com.snowdev.tweetyfall.shared.Utils.PPM;


public class SplashScreen extends ScreenClass {

    private MyImage fishImage, background;
    private MyStage stage;

    public SplashScreen(final TweetyFallGame game){
        super(game, false);

        stage = new MyStage(
                new FillViewport(game.cam.viewportWidth, game.cam.viewportHeight, game.cam)
        );
    }


    @Override
    public void showAbstract() {
        Gdx.input.setInputProcessor(stage);

        initializedActors();
        playIntroAnimation();
        //TODO: initialize font "Tap me"
    }


    private void initializedActors(){
        background = AssetsHandler.retrieveImage(AssetsHandler.MENU_BACKGROUND, true, CENTER_X, VALUE_Y, 0, 0);
        fishImage = AssetsHandler.retrieveImage(AssetsHandler.FISH,             true, CENTER_X, VALUE_Y, 0, CAMERA_HEIGHT_METER);

        stage.addActor(background);
        stage.addActor(fishImage);
    }

    private void playIntroAnimation(){
        //hide -> fade in
        stage.addAction(
                sequence(
                        alpha(0),
                        alpha(1, 2f, Interpolation.sine)
                )
        );

        //move up (hide) -> scale down -> slide down & scale up -> fade it away
        fishImage.addAction(
                sequence(
                        scaleTo(0.1f, 0.1f),
                        parallel(
                                moveTo(CAMERA_WIDTH_METER / 2f - fishImage.getWidth() / 2f, CAMERA_HEIGHT_METER / 2f - fishImage.getHeight() / 2f, 2f, Interpolation.swing),
                                scaleTo(0.7f, 0.7f, 2f, Interpolation.pow2)
                        ),
                        delay(1.5f),
                        delay(0.3f)
                )
        );
    }


    @Override
    public void render(float delta) {
        delta *= SLOWMO;

        Gdx.gl.glClearColor(LOADING_BAR_COLOR.r, LOADING_BAR_COLOR.g, LOADING_BAR_COLOR.b, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();

        update();
    }

    private void update(){
        //if the screen is tapped
        if(Gdx.input.justTouched()){
            rotateFishImage();
        }

        if(stage.finishedAnimating()){
            game.setCurrentScreen(MENU_SCREEN);
        }
    }

    private void rotateFishImage(){
        fishImage.addAction(
                parallel(
                        rotateBy(-360*3f,1f, Interpolation.sine)
                )
        );
    }


    @Override
    public void dispose() {
        System.out.println("SplashScreen dispose");
        stage.dispose();
    }
}
