package com.snowdev.tweetyfall.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.snowdev.tweetyfall.TweetyFallGame;
import com.snowdev.tweetyfall.screens.abstracts.ScreenClass;
import com.snowdev.tweetyfall.screens.abstracts.SimpleScreenClass;

import static com.snowdev.tweetyfall.models.loading_screen.AssetsHandler.retrieveFont;
import static com.snowdev.tweetyfall.shared.Constants.CAMERA_HEIGHT;
import static com.snowdev.tweetyfall.shared.Constants.CAMERA_WIDTH;
import static com.snowdev.tweetyfall.shared.Constants.SLOWMO;


/**
 * Just for testing purposes..
 */
public class TestingScreen2 extends SimpleScreenClass implements InputProcessor {

    private Stage stage;

    public TestingScreen2(TweetyFallGame game){
        super(game);
    }

    @Override
    public void show() {
        stage = new Stage(new StretchViewport(CAMERA_WIDTH, CAMERA_HEIGHT, game.cam));

        BitmapFont font = retrieveFont();
//        font.getData().scale(1/2f);

        Label label = new Label("3.00",new Label.LabelStyle(font, Color.WHITE));
//        Label label = new Label("Hjhnbnbn", new Label.LabelStyle(retrieveFont(), Color.WHITE));
        label.setDebug(true);
        label.setFontScale(1/2f);
        label.pack();


        Group group = new Group();
        group.addActor(label);
//        group.setDebug(true);
        stage.addActor(group);

//        group.setSize(label.getWidth(), label.getHeight());
//        group.setOrigin(500,500);

//        group.setPosition(CAMERA_WIDTH/2,CAMERA_HEIGHT/2);

//        group.setScale(1/2f);

//        group.addAction(
//                Actions.scaleTo(0,1,5f)
//        );

        System.out.println(group.getX() + " " + group.getY() + " " + group.getWidth() + " " + group.getHeight());
        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void render(float delta) {
        delta *= SLOWMO;

        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();
//        stage.setDebugAll(true);
    }


    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
