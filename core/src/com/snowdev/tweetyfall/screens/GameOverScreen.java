package com.snowdev.tweetyfall.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.snowdev.tweetyfall.TweetyFallGame;
import com.snowdev.tweetyfall.screens.abstracts.ScreenClass;
import com.snowdev.tweetyfall.screens.abstracts.SimpleScreenClass;

//NOT IMPLEMENTED YET
public class GameOverScreen extends SimpleScreenClass {

    public GameOverScreen(TweetyFallGame game){
        super(game);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);


        if(Gdx.input.isTouched()){
            game.setScreen(new GameScreen(game));
        }
    }

    //todo: memory improvements
    @Override
    public void dispose() {
        System.out.println("GameOverScreen dispose");
    }
}
