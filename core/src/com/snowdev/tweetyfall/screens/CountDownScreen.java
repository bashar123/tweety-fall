package com.snowdev.tweetyfall.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.snowdev.tweetyfall.TweetyFallGame;
import com.snowdev.tweetyfall.models.count_down_screen.CountDownModel;
import com.snowdev.tweetyfall.models.loading_screen.AssetsHandler;
import com.snowdev.tweetyfall.models.shared.extensions.MyImage;
import com.snowdev.tweetyfall.screens.abstracts.ScreenClass;
import com.snowdev.tweetyfall.screens.abstracts.SimpleScreenClass;

import static com.snowdev.tweetyfall.shared.Constants.PositionXE.CENTER_X;
import static com.snowdev.tweetyfall.shared.Constants.PositionYE.CENTER_Y;
import static com.snowdev.tweetyfall.shared.Constants.PositionYE.VALUE_Y;
import static com.snowdev.tweetyfall.shared.Constants.SLOWMO;

public class CountDownScreen extends SimpleScreenClass {

    private Stage uiStage;
    private CountDownModel model;

    public CountDownScreen(final TweetyFallGame game) {
        super(game);

        uiStage = new Stage(
                new StretchViewport(game.uiCam.viewportWidth, game.uiCam.viewportHeight, game.uiCam)
        );
    }

    @Override
    public void show() {

        model = new CountDownModel(game);

        MyImage background = AssetsHandler.retrieveImage(AssetsHandler.MENU_BACKGROUND, false, CENTER_X, VALUE_Y, 0, 0);
        MyImage mountains       = AssetsHandler.retrieveImage(AssetsHandler.MENU_MOUNTAINS, false, CENTER_X, CENTER_Y, 0, 0);

        uiStage.addActor(background);
        uiStage.addActor(mountains);

        for (Actor actor: model.getScreenImages()){
            uiStage.addActor(actor);
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        delta *= SLOWMO;

        model.update(delta);
        uiStage.act();
        uiStage.draw();
    }

    @Override
    public void dispose() {
        System.out.println("CountDownScreen dispose");
        uiStage.dispose();

    }


}
