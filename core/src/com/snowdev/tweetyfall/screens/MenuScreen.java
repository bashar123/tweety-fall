package com.snowdev.tweetyfall.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.snowdev.tweetyfall.TweetyFallGame;
import com.snowdev.tweetyfall.controllers.MenuController;
import com.snowdev.tweetyfall.models.menu_screen.MenuModel;
import com.snowdev.tweetyfall.models.shared.extensions.MyStage;
import com.snowdev.tweetyfall.screens.abstracts.ScreenClass;

import static com.snowdev.tweetyfall.shared.Constants.DEBUG_RENDER;
import static com.snowdev.tweetyfall.shared.Constants.SLOWMO;

public class MenuScreen extends ScreenClass {

    private MenuModel menuModel;
    private Stage stage;
    private Viewport vp;

    public MenuScreen(final TweetyFallGame game){
        super(game, true);

        vp = new StretchViewport(game.cam.viewportWidth, game.cam.viewportHeight, game.cam);
        stage = new MyStage(vp);
    }

    @Override
    public void showAbstract() {
        menuModel = new MenuModel( game );
        Gdx.input.setInputProcessor(
                new InputMultiplexer(
                        new MenuController(game),
                        stage)
        );

        populateStage();
    }

    private void populateStage(){
        for (Actor actor : menuModel.getMenuActors()){
            stage.addActor(actor);
        }
    }

    @Override
    public void render(float delta) {
        menuModel.update(delta);
        stage.act(delta);

        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.draw();

        if(DEBUG_RENDER)
            menuModel.getWorldDebugRenderer().render(menuModel.getWorld(), game.cam.combined); //todo: check this cam shit out
    }

    public void resize(int width, int height) {
        vp.update(width, height);
        //game.cam.position.y = 1.22f;
        //game.cam.update();
    }

    @Override
    public void dispose() {
        System.out.println("MenuScreen dispose");
        stage.dispose();
    }
}
