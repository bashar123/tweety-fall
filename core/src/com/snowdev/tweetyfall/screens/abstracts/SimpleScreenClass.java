package com.snowdev.tweetyfall.screens.abstracts;

import com.badlogic.gdx.Screen;
import com.snowdev.tweetyfall.TweetyFallGame;


public abstract class SimpleScreenClass implements Screen {

    protected final TweetyFallGame game;

    protected SimpleScreenClass(final TweetyFallGame game){
        this.game = game;
    }

    @Override
    public void show() {}

    @Override
    public void resize(int width, int height) {}

    @Override
    public void pause() {}

    @Override
    public void resume() {}

    @Override
    public void hide() {}

    @Override
    public void dispose(){}
}
