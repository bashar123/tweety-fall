package com.snowdev.tweetyfall.screens.abstracts;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.snowdev.tweetyfall.TweetyFallGame;
import com.snowdev.tweetyfall.shared.Utils;


public abstract class ScreenClass implements Screen {

    protected final TweetyFallGame game;
    private final boolean screenAwake;

    protected ScreenClass(final TweetyFallGame game, boolean screenAwake){
        this.game = game;
        this.screenAwake = screenAwake;
    }

    @Override
    public void show() {
        Utils.setWakeLock(game.androidInterface, screenAwake);
        Utils.showToast(game.androidInterface, Gdx.graphics.getWidth() + " : " + Gdx.graphics.getHeight());
        showAbstract();
    }

    protected abstract void showAbstract();


//    public boolean outroFinished;

    //todo: todo later (TransformScreen)
//    @Override
//    public void render(float delta) {}
//        if(playOutro())
//            outroFinished = true;
//    }

//    /**
//     * Gets called once just before hide, when {@link #render(float)} is still active
//     * @return Outro animation is finished
//     */
//    public boolean playOutro(){
//        return false;
//    }

    @Override
    public void resize(int width, int height) {}

    @Override
    public void pause() {}

    @Override
    public void resume() {}

    @Override
    public void hide() {}

    @Override
    public void dispose(){}

}
