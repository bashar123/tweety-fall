package com.snowdev.tweetyfall.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.snowdev.tweetyfall.TweetyFallGame;
import com.snowdev.tweetyfall.controllers.LoadingController;
import com.snowdev.tweetyfall.models.loading_screen.AssetsHandler;
import com.snowdev.tweetyfall.screens.abstracts.SimpleScreenClass;
import com.snowdev.tweetyfall.shared.Utils;

import static com.snowdev.tweetyfall.shared.Constants.*;
import static com.snowdev.tweetyfall.shared.Constants.ScreenE.*;


/**
 * The first screen, previews the loading bar for loading the assets
 */
public class LoadingScreen extends SimpleScreenClass {

    private AssetsHandler assetsHandler;
    private ShapeRenderer shapeRenderer;

    private float horizontalProgressPercentage, verticalProgressPercent, currentHeight, currentWidth, currentX, currentY;
    private final float BAR_HEIGHT, BAR_WIDTH, BAR_X, BAR_Y;


    //Screen wont be able to restart
    public LoadingScreen(final TweetyFallGame game){
        super(game);

        shapeRenderer = new ShapeRenderer();
        assetsHandler = new AssetsHandler();

        horizontalProgressPercentage = 0;
        verticalProgressPercent = 0;

        BAR_HEIGHT = 2;
        BAR_WIDTH = CAMERA_WIDTH - 400;
        BAR_X = 200;
        BAR_Y = 400;

        currentHeight = BAR_HEIGHT;
        currentWidth = 0;
        currentX = BAR_X;
        currentY = BAR_Y;

    }


    @Override
    public void show() {
        Gdx.input.setInputProcessor(new LoadingController(game));
        Utils.showToast(game.androidInterface, "Tap/click to show splash screen.");

        assetsHandler.loadAssets();
    }


    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0f, 0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        shapeRenderer.setProjectionMatrix(game.cam.combined);

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(LOADING_BAR_COLOR);
        shapeRenderer.rect(
                Utils.PPM(currentX),
                Utils.PPM(currentY),
                Utils.PPM(currentWidth),
                Utils.PPM(currentHeight)
        );
        shapeRenderer.end();

        update(delta);
    }


    private void update(float delta){
        boolean doneAssetsLoading = horizontalProgressPercentage + 0.001f >= 1;

        if(doneAssetsLoading ){
            boolean orangeScreenAnimationFinished = updateOrangeScreenAnimation(delta, LOADING_ANIMATION_INTERP);
            if(orangeScreenAnimationFinished) game.setCurrentScreen(SECOND_SCREEN);
        }
        else {
            updateProgressBar();
        }
    }


    private void updateProgressBar(){
        horizontalProgressPercentage = MathUtils.lerp(horizontalProgressPercentage, assetsHandler.getProgress(), 0.1f);
        currentWidth = (CAMERA_WIDTH - currentX *2) * horizontalProgressPercentage;
    }



    private boolean updateOrangeScreenAnimation(float delta, Interpolation interpolation){
        verticalProgressPercent = verticalProgressPercent + (delta * LOADING_ANIMATION_SPEED);
        verticalProgressPercent = Math.min(1f, verticalProgressPercent); //So the percentage won't exceed 100%
        //                  from          To                  //percentage
        currentHeight =     BAR_HEIGHT + (CAMERA_HEIGHT *     interpolation.apply(verticalProgressPercent));
        currentWidth =      BAR_WIDTH +  (BAR_X *             interpolation.apply(verticalProgressPercent)) * 2;
        currentX =          BAR_X -      (BAR_X *             interpolation.apply(verticalProgressPercent));
        currentY =          BAR_Y -      (BAR_Y *             interpolation.apply(verticalProgressPercent));

        return currentHeight >= CAMERA_HEIGHT; // True if the animation is finished
    }

    @Override
    public void dispose() {
        System.out.println("LoadingScreen dispose");
        assetsHandler.dispose();
        shapeRenderer.dispose();
    }
}
