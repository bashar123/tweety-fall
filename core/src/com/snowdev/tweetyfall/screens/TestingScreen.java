package com.snowdev.tweetyfall.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.snowdev.tweetyfall.TweetyFallGame;
import com.snowdev.tweetyfall.models.loading_screen.AssetsHandler;
import com.snowdev.tweetyfall.screens.abstracts.ScreenClass;
import com.snowdev.tweetyfall.screens.abstracts.SimpleScreenClass;

import static com.snowdev.tweetyfall.shared.Constants.CAMERA_HEIGHT;
import static com.snowdev.tweetyfall.shared.Constants.CAMERA_WIDTH;


/**
 * USE TESTINGSCREEN2 for TESTING
 * Just for testing purposes, cam, viewport..
 */
public class TestingScreen extends SimpleScreenClass implements InputProcessor {

    private final int VIR_CAMERA_WIDTH =  (int) CAMERA_WIDTH/2;
    private final int VIR_CAMERA_HEIGHT =  (int) CAMERA_HEIGHT/2;

    private OrthographicCamera cam;
    private SpriteBatch batch;
    private Sprite image;
//    private Viewport viewport;

    public TestingScreen(TweetyFallGame game){
        super(game);
    }

    @Override
    public void show() {
        cam = new OrthographicCamera();
        cam.setToOrtho(false, VIR_CAMERA_WIDTH,VIR_CAMERA_HEIGHT);
//        viewport = new StretchViewport(CAMERA_WIDTH, CAMERA_HEIGHT, cam);

        image = new Sprite(new Texture(AssetsHandler.BAD_LOGO));
        image.setSize(1080, 1920);

        batch = new SpriteBatch();
        batch.setProjectionMatrix(cam.combined);


        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
            image.draw(batch);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {
//        viewport.update(width/2,height/2);
    }

    @Override
    public void dispose() {

    }


    @Override
    public boolean keyDown(int keycode) {
        System.out.println("GDX width: " + Gdx.graphics.getWidth()   + "\t\t\t- GDX height: " + Gdx.graphics.getHeight()); //changes with screen size
        System.out.println("Camera VP width: " + cam.viewportWidth   + "\t- Camera VP height: " + cam.viewportHeight); //dosent change with screen size
        System.out.println("MyImage X: " + image.getX()                + "\t\t\t- MyImage Y: " + image.getY());
        System.out.println("MyImage width: " + image.getWidth()        + "\t\t- MyImage height: " + image.getHeight());
        System.out.println("------------------------------------------------");

        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
