package com.snowdev.tweetyfall;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.snowdev.tweetyfall.android.AndroidInterface;
import com.snowdev.tweetyfall.screens.*;
import com.snowdev.tweetyfall.shared.Utils;

import static com.snowdev.tweetyfall.shared.Constants.*;
import static com.snowdev.tweetyfall.shared.Constants.ScreenE.*;

public class TweetyFallGame extends Game {
	public OrthographicCamera cam, uiCam;
	public AndroidInterface androidInterface;

    private Screen loadingScreen, splashScreen, menuScreen, countDownScreen, gameScreen, gameOverScreen, testingScreen, testingScreen2;

    //Gets called from desktop app
	public TweetyFallGame(){
        DESKTOP_VERSION = true;
    }

    //Gets called from Android app
	public TweetyFallGame(AndroidInterface androidInterface){
		this.androidInterface = androidInterface;
		this.androidInterface.screenOn();
	}

	@Override
	public void create () {
		cam = new OrthographicCamera(CAMERA_WIDTH_METER , CAMERA_HEIGHT_METER );
		cam.update();
		uiCam = new OrthographicCamera(CAMERA_WIDTH , CAMERA_HEIGHT );
		uiCam.update();

		loadingScreen 	= new LoadingScreen(this);
		splashScreen 	= new SplashScreen(this);
		menuScreen		= new MenuScreen(this);
		countDownScreen = new CountDownScreen(this);
		gameScreen      = new GameScreen(this);
		gameOverScreen  = new GameOverScreen(this);
		testingScreen	= new TestingScreen(this);
		testingScreen2	= new TestingScreen2(this);

		setCurrentScreen(LOADING_SCREEN);
	}


	public void setCurrentScreen( ScreenE screen ){
		switch (screen){
			case LOADING_SCREEN :
				setScreen(loadingScreen);
				break;

			case SPLASH_SCREEN :
				setScreen(splashScreen);
				break;

			case MENU_SCREEN :
				setScreen(menuScreen);
				break;

			case COUNT_DOWN_SCREEN:
				setScreen(countDownScreen);
				break;

			case GAME_SCREEN :
				setScreen(gameScreen);
				break;

			case GAMEOVER_SCREEN :
				setScreen(gameOverScreen);
				break;

			case TESTING_SCREEN :
				setScreen(testingScreen);
				break;

			case TESTING_SCREEN2 :
				setScreen(testingScreen2);
				break;
		}
	}


	@Override
	public void dispose() {
		System.out.println("TweetyFallGame dispose");

		if(androidInterface != null)
			androidInterface.screenOff();

        loadingScreen.dispose();
		splashScreen.dispose();
		menuScreen.dispose();
		gameScreen.dispose();
		gameOverScreen.dispose();
	}
}
