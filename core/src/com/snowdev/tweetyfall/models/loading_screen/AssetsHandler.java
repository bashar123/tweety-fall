package com.snowdev.tweetyfall.models.loading_screen;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Array;
import com.snowdev.tweetyfall.models.shared.extensions.MyImage;


import static com.snowdev.tweetyfall.shared.Constants.PositionXE.VALUE_X;
import static com.snowdev.tweetyfall.shared.Constants.PositionYE.VALUE_Y;
import static com.snowdev.tweetyfall.shared.Constants.PositionYE;
import static com.snowdev.tweetyfall.shared.Constants.PositionXE;

import java.util.Arrays;
import java.util.List;


import static com.snowdev.tweetyfall.shared.Constants.THEME;

public class AssetsHandler {

    public static final String FISH                     = THEME + "/Splash/fish.png";
    public static final String BAD_LOGO                 = THEME + "/Game/bad.jpg";
    public static final String GAME_FONT                = THEME + "/Game/font/font.fnt";
    public static final String BLANK                    = THEME + "/Game/blank.png";

    public static final String SPLASH_BACKGROUND        = THEME + "/Splash/background.png";

    public static final String MENU_BACKGROUND          = THEME + "/Menu/blurred-background.png";
    public static final String MENU_MOON                = THEME + "/Menu/blurred-moon.png";
    public static final String MENU_MOUNTAINS           = THEME + "/Menu/blurred-mountains.png";
    public static final String MENU_PLAY                = THEME + "/Menu/play-button.png";
    public static final String MENU_SETTINGS_BUTTON     = THEME + "/Menu/settings-button.png";
    public static final String MENU_SETTINGS_BACKGROUND = THEME + "/Menu/settings-background.png";
    public static final String MENU_TITLE               = THEME + "/Menu/Title/letter";
    public static final String MENU_TITLE_FULL          = THEME + "/Menu/tweetyfall-text.png";
    public static final String MENU_MUTED               = THEME + "/Menu/muted.png";
    public static final String MENU_UNMUTED             = THEME + "/Menu/un-muted.png";

    public static final String MENU_BLUE_BIRD           = THEME + "/Menu/blurredBlueBird.png";
    public static final String MENU_RED_BIRD            = THEME + "/Menu/blurredRedBird.png";
    public static final String MENU_RATE                = THEME + "/Menu/rate-button.png";
    public static final String MENU_INFO                = THEME + "/Menu/info-button.png";
    public static final String MENU_SHADOW_OVERLAY      = THEME + "/Menu/shadow-overlay.png";
    public static final String MENU_ABOUT_BACKGROUND    = THEME + "/Menu/about-background.png";
    public static final String MENU_ABOUT_SUBMIT_BUTTON = THEME + "/Menu/submit-button.png";


    public static final String GAME_MOON                = THEME + "/Game/moon.png";
    public static final String GAME_BACKGROUND          = THEME + "/Game/background.png";
    public static final String GAME_NEST_BLUE           = THEME + "/Game/nest-blue.png";
    public static final String GAME_NEST_PURPLE         = THEME + "/Game/nest-purple.png";
    public static final String GAME_NEST_YELLOW         = THEME + "/Game/nest-yellow.png";
    public static final String GAME_GROUND              = THEME + "/Game/ground.png";
    public static final String GAME_STAR                = THEME + "/Game/star.png";
    public static final String GAME_UPPER_CLOUDS_1      = THEME + "/Game/clouds/UpperClouds1Copy1.png";
    public static final String GAME_UPPER_CLOUDS_2      = THEME + "/Game/clouds/UpperClouds1Copy2.png";
    public static final String GAME_UPPER_CLOUDS_3      = THEME + "/Game/clouds/UpperClouds1Copy3.png";
    public static final String GAME_UPPER_CLOUDS_4      = THEME + "/Game/clouds/UpperClouds1Copy4.png";
    public static final String GAME_LOWER_CLOUDS_1      = THEME + "/Game/clouds/UpperClouds2Copy1.png";
    public static final String GAME_LOWER_CLOUDS_2      = THEME + "/Game/clouds/UpperClouds2Copy2.png";
    public static final String GAME_LOWER_CLOUDS_3      = THEME + "/Game/clouds/UpperClouds2Copy3.png";
    public static final String GAME_LOWER_CLOUDS_4      = THEME + "/Game/clouds/UpperClouds2Copy4.png";
    public static final String GAME_MOUNTAINS           = THEME + "/Game/mountains.png";
    public static final String GAME_HORIZON             = THEME + "/Game/horizon.png";
    public static final String GAME_BLUE_BIRD_ATLAS     = THEME + "/Game/bird-animation-blue/BirdSpriteSheet.atlas";
    public static final String GAME_RED_BIRD_ATLAS      = THEME + "/Game/bird-animation-red/birdAnimationRed.atlas";

    public static final String GAME_ATLAS_NEST_PURPLE_BLUE      = THEME + "/Game/nest-purple_blue/nest-purple_blue.atlas";
    public static final String GAME_ATLAS_NEST_BLUE_YELLOW      = THEME + "/Game/nest-blue_yellow/nest-blue_yellow.atlas";
    public static final String GAME_ATLAS_NEST_YELLOW_PURPLE    = THEME + "/Game/nest-yellow_purple/nest-yellow_purple.atlas";

    public static final String GAME_BLUE_BIRD           = THEME + "/Game/bird-blue.png";
    public static final String GAME_RED_BIRD            = THEME + "/Game/bird-red.png";

    public static final String POINT_SOUND              = "Audio/point.wav" ;
    public static final String LOSE_SOUND               = "Audio/lose.wav" ;
    public static final String BACKGROUND_MUSIC         = "Audio/background.ogg";


    private static AssetManager assets;


    public AssetsHandler (){
        assets = new AssetManager();
    }

    public static <T> T getAsset(String fileName, Class<T> type){
        return assets.get(fileName, type);
    }

    public static MyImage retrieveImage(String imageName, boolean convertToMeter, PositionXE posXE, PositionYE posYE, float x, float y){
        return new MyImage(assets.get(imageName, Texture.class), convertToMeter, posXE, posYE, x, y);
    }

    public static MyImage retrieveImage(String imageName, boolean convertToMeter){
        return new MyImage(assets.get(imageName, Texture.class), convertToMeter);
    }

    public static BitmapFont retrieveFont(){
        return assets.get(GAME_FONT, BitmapFont.class);
    }

    public static Array<MyImage> createImages(String imagesName, int numberOfSprites, boolean convertToMeter){
        Array<MyImage> images = new Array<>();

        for (int i = 0; i< numberOfSprites; i++)
            images.add(new MyImage(assets.get(imagesName + i + ".png", Texture.class), convertToMeter, VALUE_X, VALUE_Y, 0, 0));

        return images;
    }

    //todo
//    public static Animation createAnimation(){
//        return new Animation();
//    }



    public void loadAssets(){

        final Array<String> textureArray = new Array<>();
        textureArray.addAll(
            FISH, BAD_LOGO, BLANK, SPLASH_BACKGROUND,

            MENU_BACKGROUND, MENU_MOON, MENU_MOUNTAINS, MENU_PLAY, MENU_SETTINGS_BUTTON, MENU_SETTINGS_BACKGROUND,
            MENU_TITLE_FULL, MENU_BLUE_BIRD, MENU_RED_BIRD, MENU_RATE, MENU_INFO, MENU_SHADOW_OVERLAY,
            MENU_MUTED, MENU_UNMUTED, MENU_ABOUT_BACKGROUND, MENU_ABOUT_SUBMIT_BUTTON,

            GAME_MOON, GAME_BACKGROUND, GAME_NEST_BLUE, GAME_NEST_PURPLE, GAME_NEST_YELLOW, GAME_GROUND, GAME_STAR,
            GAME_UPPER_CLOUDS_1, GAME_UPPER_CLOUDS_2, GAME_UPPER_CLOUDS_3, GAME_UPPER_CLOUDS_4,
            GAME_LOWER_CLOUDS_1, GAME_LOWER_CLOUDS_2, GAME_LOWER_CLOUDS_3, GAME_LOWER_CLOUDS_4,
            GAME_MOUNTAINS, GAME_HORIZON, GAME_BLUE_BIRD, GAME_RED_BIRD
        );

        for (int i = 0; i < 10; i++){
            String s = MENU_TITLE + i + ".png";
            textureArray.add(s);
        }


        final List<String> textureAtlases = Arrays.asList(
                GAME_BLUE_BIRD_ATLAS, GAME_RED_BIRD_ATLAS, GAME_ATLAS_NEST_PURPLE_BLUE,
                GAME_ATLAS_NEST_BLUE_YELLOW, GAME_ATLAS_NEST_YELLOW_PURPLE
        );


        //LOADING ASSETS
        for(String t : textureArray)
            assets.load(t, Texture.class);

        for(String textureAtlas : textureAtlases)
            assets.load(textureAtlas, TextureAtlas.class);

        //Other assets
        assets.load(POINT_SOUND, Sound.class);
        assets.load(LOSE_SOUND, Sound.class);
        assets.load(BACKGROUND_MUSIC, Music.class);
        assets.load(GAME_FONT, BitmapFont.class);
    }

    public float getProgress(){
        assets.update();
        return assets.getProgress();
    }

    public void dispose(){
        assets.dispose();
    }

}
