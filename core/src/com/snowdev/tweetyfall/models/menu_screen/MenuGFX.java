package com.snowdev.tweetyfall.models.menu_screen;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Array;
import com.snowdev.tweetyfall.models.loading_screen.AssetsHandler;
import com.snowdev.tweetyfall.models.shared.BirdBodyUserData;
import com.snowdev.tweetyfall.models.shared.extensions.MyImage;
import com.snowdev.tweetyfall.shared.Utils;

import java.util.Iterator;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;
import static com.snowdev.tweetyfall.models.loading_screen.AssetsHandler.createImages;
import static com.snowdev.tweetyfall.shared.Constants.BIRD_IMAGE_POOL_SIZE;
import static com.snowdev.tweetyfall.shared.Constants.CAMERA_HEIGHT_METER;
import static com.snowdev.tweetyfall.shared.Constants.CAMERA_WIDTH_METER;
import static com.snowdev.tweetyfall.shared.Constants.ColorE.BLUE_BIRD;
import static com.snowdev.tweetyfall.shared.Constants.ColorE.RED_BIRD;
import static com.snowdev.tweetyfall.shared.Constants.GAME_ANIMATION_INTERP;
import static com.snowdev.tweetyfall.shared.Constants.PositionXE.CENTER_X;
import static com.snowdev.tweetyfall.shared.Constants.PositionXE.LEFT;
import static com.snowdev.tweetyfall.shared.Constants.PositionXE.VALUE_X;
import static com.snowdev.tweetyfall.shared.Constants.PositionYE.BOTTOM;
import static com.snowdev.tweetyfall.shared.Constants.PositionYE.VALUE_Y;
import static com.snowdev.tweetyfall.shared.Utils.PPM;


/**
 * Initiates all the images (GFX) and plays intro/outro
 */
public class MenuGFX {
    private MyImage background, mountains, moon;
    MyImage play, settings, settingsBackground, about, shadowOverlay, titleFull, muted, unmuted, aboutBackground, submit; //Clickable's; needs access from MenuModel
    Array<MyImage> blueBirdPool, redBirdPool, titleArray;
//    Group countDownGroup;

    private boolean playAlreadyClicked;

    MenuGFX(){
        blueBirdPool = new Array<>();
        redBirdPool = new Array<>();

        initiateImages();
        playIntro();
    }


    private void initiateImages(){
        shadowOverlay   = AssetsHandler.retrieveImage(AssetsHandler.MENU_SHADOW_OVERLAY, true, CENTER_X, VALUE_Y, 0, 0);
        background      = AssetsHandler.retrieveImage(AssetsHandler.MENU_BACKGROUND, true, CENTER_X, VALUE_Y, 0, 0);
        moon            = AssetsHandler.retrieveImage(AssetsHandler.MENU_MOON, true, LEFT, VALUE_Y, 0, 0);
        mountains       = AssetsHandler.retrieveImage(AssetsHandler.MENU_MOUNTAINS, true, CENTER_X, BOTTOM, 0, 0);
        titleFull       = AssetsHandler.retrieveImage(AssetsHandler.MENU_TITLE_FULL, true, CENTER_X, VALUE_Y, 0, PPM(1310)); //used for debugging
        play            = AssetsHandler.retrieveImage(AssetsHandler.MENU_PLAY, true, CENTER_X, VALUE_Y, 0,  PPM(800));

        initBackgroundBirds();
        initTitle();

        float backgroundY = PPM(235);
        initSetting(PPM(100), backgroundY);
        initAbout(PPM(100), backgroundY);
    }



    private void initBackgroundBirds() {
        for (int i = 0; i < BIRD_IMAGE_POOL_SIZE; i++){
            blueBirdPool.add(AssetsHandler.retrieveImage(AssetsHandler.MENU_BLUE_BIRD, true, VALUE_X, VALUE_Y, -1, -1));
            redBirdPool.add(AssetsHandler.retrieveImage(AssetsHandler.MENU_RED_BIRD, true, VALUE_X, VALUE_Y, -1, -1));
        }
    }

    private void initTitle(){
        titleArray  = createImages(AssetsHandler.MENU_TITLE, 10, true);

        //position title
        Array<Integer> xPositions = new Array<>();
        xPositions.addAll(114,275,420,539,648,777, 276, 400, 515, 620);

        float firstRowYPos = CAMERA_HEIGHT_METER + PPM(200);
        float secondRowYPos = firstRowYPos - PPM(200);

        for(int i = 0; i < xPositions.size; i++){
            if(i<6) //first title row
                titleArray.get(i).setY(firstRowYPos);
            else //second title row
                titleArray.get(i).setY(secondRowYPos);

            titleArray.get(i).setX(PPM(xPositions.get(i)));
        }
    }

    private void initAbout(float buttonRightPadding, float aboutBackgroundY) {
        about = AssetsHandler.retrieveImage(AssetsHandler.MENU_INFO, true, VALUE_X, BOTTOM, 0, 0);
        about.setX(CAMERA_WIDTH_METER - about.getWidth() - buttonRightPadding);
        about.setScale(0.8f, 0.8f);

        aboutBackground = AssetsHandler.retrieveImage(AssetsHandler.MENU_ABOUT_BACKGROUND, true, CENTER_X, VALUE_Y, 0, aboutBackgroundY);

        submit = AssetsHandler.retrieveImage(AssetsHandler.MENU_ABOUT_SUBMIT_BUTTON, true, VALUE_X, VALUE_Y, PPM(115), PPM(300));
    }

    private void initSetting(float buttonLeftPadding, float settingsBackgroundY) {
        settings    = AssetsHandler.retrieveImage(AssetsHandler.MENU_SETTINGS_BUTTON, true, VALUE_X, BOTTOM, buttonLeftPadding,  0);
        settings.setScale(0.8f,0.8f);

        settingsBackground = AssetsHandler.retrieveImage(AssetsHandler.MENU_SETTINGS_BACKGROUND, true, VALUE_X, VALUE_Y, settings.getX() + PPM(5),  settingsBackgroundY);

        muted = AssetsHandler.retrieveImage(AssetsHandler.MENU_MUTED, true, VALUE_X, VALUE_Y, settings.getX(),  PPM(155));
        muted.setScale(0.4f, 0.4f);

        unmuted = AssetsHandler.retrieveImage(AssetsHandler.MENU_UNMUTED, true, VALUE_X, VALUE_Y, muted.getX(),  muted.getY());
        unmuted.setScale(0.4f, 0.4f);
    }



    /**
     * Intro animations to menu screen
     */
    private void playIntro(){
        float introDuration = 1f;

        //hide
        shadowOverlay.fadeOut();
        play.scaleBy(-1);

        //show
        animateTitle(true);

        shadowOverlay.addAction(
                alpha(0.5f, introDuration)
        );

        moon.addAction(moveTo(moon.getCenterX(), moon.getY(), introDuration, GAME_ANIMATION_INTERP));
        mountains.addAction(moveTo(mountains.getX(), 0, introDuration, GAME_ANIMATION_INTERP));
        play.addAction(
                sequence(
                    delay(0.5f),
                    scaleBy(1,1, introDuration, GAME_ANIMATION_INTERP)
                )
        );
        settings.addAction(
                sequence(
                        delay(MathUtils.random(0.1f, 0.3f)),
                        moveTo(settings.getX(), Utils.PPM(100), introDuration, GAME_ANIMATION_INTERP) //dy 50 -> 100
                )
        );

        about.addAction(
                parallel(
                        sequence(
                                delay(MathUtils.random(0.1f, 0.3f)),
                                moveTo(about.getX(), Utils.PPM(100), introDuration, GAME_ANIMATION_INTERP) //dy 20 -> 60
                        )

                )
        );
    }

    /**
     * When Play button pressed
     */
    void playOutroAnimations(){
        
        if(playAlreadyClicked)
           return;

        animateTitle(false);

        slideBirdsToTheSideOfScreen(blueBirdPool, true);
        slideBirdsToTheSideOfScreen(redBirdPool, false);

        moon.addAction(moveBy(-moon.getWidth(), 0, 1f, Interpolation.exp5));
        shadowOverlay.addAction(fadeOut(1));

        play.addAction(scaleTo(0,0, 0.5f, Interpolation.exp5In));


        settings.addAction(
                sequence(
                        moveTo(settings.getX(), -settings.getHeight(), MathUtils.random(0.2f, 0.4f), Interpolation.exp5)
                )
        );
        about.addAction(
                sequence(
                        moveTo(about.getX(), -about.getHeight(), MathUtils.random(0.2f, 0.4f), Interpolation.exp5)
                )
        );

        playAlreadyClicked = true;
    }

    private void animateTitle(boolean slideDown){
        int distance = 600;
        for(MyImage letter: titleArray){
            if(slideDown) {
                float delay = MathUtils.random(0.6f, 1.0f);
                letter.addAction(Actions.moveBy(0, PPM(-distance), delay, GAME_ANIMATION_INTERP));
            }
            else { // slideUp
                float delay = MathUtils.random(0.3f,0.5f);
                letter.addAction(
                        sequence (
                                moveBy(0, PPM(distance), delay, Interpolation.exp5)
                        )
                );
            }
        }
    }

    private void slideBirdsToTheSideOfScreen(Array<MyImage> birdImages, boolean rightSide){
        for(MyImage birdImage: birdImages){

            float delay = MathUtils.random(0.2f,0.6f);
            Interpolation interp = Interpolation.exp5Out;

            if(birdImage.getY()==-1 && birdImage.getX()!=-1){
                System.out.println("dasdasda");
            }

            if(rightSide)
                birdImage.addAction( //move blue birds to right (out of screen)
                            moveTo(CAMERA_WIDTH_METER, birdImage.getY(), delay, interp)
                );
            else {
                birdImage.addAction( //move red birds to left (out of screen)
                        moveTo(-birdImage.getWidth(), birdImage.getY(), delay, interp)
                );
            }
        }
    }

    boolean outroAnimationIsFinished(){
        return play.isDoneAnimating();
    }


    /**
     * Background pressed (work in progress)
     */
    void playScaleEffect(){
        float scale = 0.2f, delay = 0.5f;
        Interpolation interp = Interpolation.pow3In;

        for(MyImage birdImage: blueBirdPool){
            birdImage.addAction(
                    sequence(
                            scaleBy(scale, scale, delay, interp),
                            scaleBy(-scale, -scale, delay, interp)
                    )
            );
        }
        for(MyImage birdImage: redBirdPool){
            birdImage.addAction(
                    sequence(
                        scaleBy(scale, scale, delay, interp),
                        scaleBy(-scale, -scale, delay, interp)
                    )
            );
        }
    }


    void updateBirdImageToBirdBodyPos(Array<Body> birdBodies){
        placeBirdImagesOutOfScreen(blueBirdPool);
        placeBirdImagesOutOfScreen(redBirdPool);

        Iterator<MyImage> blueItr = blueBirdPool.iterator();
        Iterator<MyImage> redItr = redBirdPool.iterator();

        for (Body birdBody: birdBodies) {

            BirdBodyUserData birdUserData = (BirdBodyUserData) birdBody.getUserData();

            float birdBodyX = birdBody.getPosition().x - birdUserData.getWidth();
            float birdBodyY = birdBody.getPosition().y - birdUserData.getHeight();


            if (birdUserData.getColor() == BLUE_BIRD){
                MyImage blueBird = blueItr.next();
                blueBird.setPosition(birdBodyX, birdBodyY );
                blueBird.setRotation(birdBody.getAngle() * MathUtils.radiansToDegrees);
            }

            else if (birdUserData.getColor() == RED_BIRD){
                MyImage redBird = redItr.next();
                redBird.setPosition(birdBodyX, birdBodyY );
                redBird.setRotation(birdBody.getAngle() * MathUtils.radiansToDegrees);
            }

            else {
                throw new IllegalArgumentException("Unknown bird color: " + birdUserData.getColor());
            }
        }
    }

    private void placeBirdImagesOutOfScreen(Array<MyImage> array){ //position Bird images offscreen
        for (MyImage bird: array){
            bird.setPosition(-100,-100); //-1 in meter..
        }
    }


    Array<Actor> getMenuActors(){
        Array<Actor> allMenuActors;
        allMenuActors = new Array<>();

        allMenuActors.addAll(
                background,
                mountains,
                moon
        );
        allMenuActors.addAll(
                blueBirdPool
        );
        allMenuActors.addAll(
                redBirdPool
        );
        allMenuActors.addAll(
                shadowOverlay
//                ,titleFull
        );
        allMenuActors.addAll(
                titleArray
        );
        allMenuActors.addAll(
                play,
                settingsBackground,
                muted,
                unmuted,
                settings,
                about,
                aboutBackground,
                submit
        );
        return allMenuActors;
    }

}
