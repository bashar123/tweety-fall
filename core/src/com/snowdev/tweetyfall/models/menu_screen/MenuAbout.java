package com.snowdev.tweetyfall.models.menu_screen;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.utils.Array;
import com.snowdev.tweetyfall.models.shared.extensions.MyImage;


import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.forever;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;
import static com.snowdev.tweetyfall.shared.Utils.PPM;

class MenuAbout {

    private boolean aboutCollapsed = true;
    private MyImage aboutButton, aboutBackground, play, submit, shadow;
    private Array<MyImage> titleArray;

    private float animationDuration = 0.5f;
    private Interpolation animationInterp = Interpolation.pow2;


    MenuAbout(MenuGFX menuGFX) {
        aboutBackground = menuGFX.aboutBackground;
        play = menuGFX.play;
        titleArray = menuGFX.titleArray;
        aboutButton = menuGFX.about;
        submit = menuGFX.submit;
        shadow = menuGFX.shadowOverlay;

        init();
    }

    private void init() {
        float origin = 1.235f;
        aboutBackground.setOrigin(aboutBackground.getWidth() / origin, 0);
        aboutBackground.setScale(0);
        aboutBackground.fadeOut();
        submit.setOrigin(submit.getWidth() / origin, 0);
        submit.setScale(0);
        submit.fadeOut();
    }


    void toggleAbout() {
        if (aboutCollapsed)
            showAboutDialog();
        else
            collapseAbout(false);
    }


    private void showAboutDialog() {
        if (!aboutCollapsed)
            return;

        aboutCollapsed = false;

        resizeTitle(true, 0.18f);
        resizePlay(true, 0.18f);
        loopHeartBeat(true);
        adjustShadow(true);

        //show aboutBackground page
        float aboutBackgroundScale = 1;
        aboutBackground.addAction(

                parallel(
                        fadeIn(animationDuration, animationInterp),
                        scaleBy(aboutBackgroundScale, aboutBackgroundScale, animationDuration, animationInterp),
                        run(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        animateSubmitButton(true);
                                    }
                                }
                        )

                )

        );
    }

    void collapseAbout(boolean byPressingPlayButton) {
        if (aboutCollapsed)
            return;

        aboutCollapsed = true;

        if (!byPressingPlayButton) {
            resizeTitle(false, 0.18f);
            resizePlay(false, 0.18f);
            loopHeartBeat(false);
        }

        adjustShadow(false);

        //hide aboutBackground page
        float aboutBackgroundScale = -1;
        aboutBackground.addAction(
                sequence(

                        parallel(
                                fadeOut(animationDuration, animationInterp),
                                scaleBy(aboutBackgroundScale, aboutBackgroundScale, animationDuration, animationInterp),
                                run(
                                    new Runnable() {
                                        @Override
                                        public void run() {
                                            animateSubmitButton(false);
                                        }
                                    }
                                )
                        )

                )
        );
    }


    private void resizeTitle(boolean minimize, float delay) {

        float firstRowY, secondRowY, titleScale;
        Array<Float> xVals = new Array<>();

        if (minimize) {
            firstRowY = PPM(70);
            secondRowY = PPM(130);
            xVals.addAll(100f, 50f, 10f, -30f, -60f, -100f, 50f, 10f, -20f, -55f);
            titleScale = 0.7f;
        } else {
            firstRowY = PPM(-70);
            secondRowY = PPM(-130);
            xVals.addAll(-100f, -50f, -10f, 30f, 60f, 100f, -50f, -10f, 20f, 55f);
            titleScale = 1;
        }

        for (int i = 0; i < titleArray.size; i++) {
            float y = i < 6 ? firstRowY : secondRowY;
            titleArray.get(i).addAction(
                    sequence(
                            delay(delay),
                            parallel(
                                    scaleTo(titleScale, titleScale, animationDuration / 2f, animationInterp),
                                    moveBy(PPM(xVals.get(i)), y, animationDuration / 2f, animationInterp)
                            )
                    )
            );
        }

    }

    private void resizePlay(boolean minimize, float delay) {
        float scale, distanceY, duration = animationDuration / 2f;

        if (minimize) {
            scale = 0.7f;
            distanceY = 330;
        } else {
            scale = 1f;
            distanceY = -330;
        }

        play.addAction(
                sequence(
                        delay(delay),
                        parallel(
                                scaleTo(scale, scale, duration, animationInterp),
                                moveBy(0, PPM(distanceY), duration, animationInterp)
                        )
                )
        );
    }

    private void animateSubmitButton(boolean show) {

        submit.clearActions();

        float animationScale            = show ? 1 : 0;
        float duration                  = show ? animationDuration : animationDuration / 2;
        Action fade                     = show ? fadeIn(duration , animationInterp) : fadeOut(duration , animationInterp);

        submit.addAction(
                parallel(
                        scaleTo(animationScale, animationScale, duration , animationInterp),
                        fade
                )
        );
    }

    private void loopHeartBeat(boolean start) {
        if (start) {
            float heartbeatScale = 0.15f;
            aboutButton.addAction(
                    forever(
                            sequence( //heart beating
                                    scaleBy(heartbeatScale, heartbeatScale, 0.4f),
                                    scaleBy(-heartbeatScale, -heartbeatScale, 0.2f)
                            )
                    )
            );
        } else {
            aboutButton.clearActions();
            aboutButton.setScale(0.7f);
        }
    }

    private void adjustShadow(boolean max){

        float value = max ? 1 : 0.5f;

        shadow.addAction(
                alpha(value,0.3f)
        );
    }

}
