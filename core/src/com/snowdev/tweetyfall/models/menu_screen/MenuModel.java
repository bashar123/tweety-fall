package com.snowdev.tweetyfall.models.menu_screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.snowdev.tweetyfall.TweetyFallGame;
import com.snowdev.tweetyfall.models.shared.extensions.MyImage;
import com.snowdev.tweetyfall.shared.Constants;

import static com.snowdev.tweetyfall.shared.Constants.GOOGLE_PLAY_URI;

//         MS = MenuScreen
//         MM = MenuModel
//
//         MS has MM & calls MM.update
//         MM initialize :
//              -   MenuGFX
//              -   MenuWorld
//              -   Settings
//              -   About

/**
 * Updates Images with new position of the nestBody
 * Button listeners
 */
public class MenuModel {

    private TweetyFallGame game;

    private MenuGFX menuGFX;
    private MenuWord menuWord;
    private MenuSettings menuSettings;
    private MenuAbout menuAbout;


    private boolean playButtonClicked;

    public MenuModel( TweetyFallGame game ){
        this.game = game;

        menuWord = new MenuWord();
        menuGFX = new MenuGFX();

        menuSettings = new MenuSettings(menuGFX);
        menuAbout = new MenuAbout(menuGFX);

        initiateListeners();
    }


    // ----------------------------------- Listeners --------------------------------------
    private void initiateListeners(){
        for (MyImage img : menuGFX.titleArray){

            img.addListener(new ClickListener(){
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    titlePressed();
                }
            });
        }

        menuGFX.play.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                playButtonPressed();
            }
        });

        menuGFX.about.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                aboutButtonPressed();
            }
        });

        menuGFX.aboutBackground.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                aboutBackgroundPressed();
            }
        });

        menuGFX.submit.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                submitButtonPressed();
            }
        });

        menuGFX.settings.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                settingsButtonPressed();
            }
        });

        menuGFX.muted.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mutedButtonPressed();
            }
        });

        menuGFX.unmuted.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                unmutedButtonPressed();
            }
        });

        menuGFX.shadowOverlay.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                backgroundPressed();
            }
        });
    }



    private void titlePressed(){
        System.out.println("titlePressed");
        menuSettings.collapseSettings();
        menuAbout.collapseAbout(false);
    }

    private void playButtonPressed(){
        System.out.println("playButtonPressed");
        playButtonClicked = true;
        menuSettings.collapseSettings();
        menuAbout.collapseAbout(true);
        menuGFX.playOutroAnimations();
    }

    private void aboutButtonPressed(){
        System.out.println("aboutButtonPressed");
        menuSettings.collapseSettings();
        menuAbout.toggleAbout();
    }

    private void aboutBackgroundPressed() {
        System.out.println("aboutBackgroundPressed");
        menuAbout.collapseAbout(false);
    }

    private void submitButtonPressed(){
        System.out.println("submitButtonPressed");
        Gdx.net.openURI(GOOGLE_PLAY_URI);
        menuAbout.collapseAbout(false);
    }

    private void settingsButtonPressed(){
        System.out.println("settingsButtonPressed");
        menuAbout.collapseAbout(false);
        menuSettings.toggleSettings();
    }

    private void mutedButtonPressed(){
        System.out.println("mutedButtonPressed");
        menuSettings.toggleMute(true);
    }

    private void unmutedButtonPressed(){
        System.out.println("unmutedButtonPressed");
        menuSettings.toggleMute(false);
    }

    private void backgroundPressed(){
        System.out.println("backgroundPressed");
        menuSettings.collapseSettings();
        menuAbout.collapseAbout(false);
        menuGFX.playScaleEffect();
    }

    // ----------------------------------- End of listeners --------------------------------------
    public Array<Actor> getMenuActors(){
        return menuGFX.getMenuActors();
    }


    public World getWorld(){
        return menuWord.world;
    }

    public Box2DDebugRenderer getWorldDebugRenderer(){
        return menuWord.debugRenderer;
    }

    public void update(float delta){
        menuWord.update(delta, 0, null);

        if(menuGFX.outroAnimationIsFinished() && playButtonClicked)
            game.setCurrentScreen(Constants.ScreenE.COUNT_DOWN_SCREEN);

        else if (!playButtonClicked)
            menuGFX.updateBirdImageToBirdBodyPos(menuWord.birdBodies); //birds falling
    }

}