package com.snowdev.tweetyfall.models.menu_screen;


import com.badlogic.gdx.math.Interpolation;
import com.snowdev.tweetyfall.models.shared.extensions.MyImage;
import com.snowdev.tweetyfall.models.shared.models.AudioModel;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.forever;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.hide;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.rotateBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.show;
import static com.snowdev.tweetyfall.shared.Utils.PPM;

class MenuSettings { //todo refactor like in MenuAbout

    private boolean settingsCollapsed = true;

    private MyImage settings, settingsBackground, muted, unmuted;


    MenuSettings(MenuGFX menuGFX){
        this.settings = menuGFX.settings;
        this.muted = menuGFX.muted;
        this.unmuted = menuGFX.unmuted;
        this.settingsBackground = menuGFX.settingsBackground;

        initSettings();
    }


    private void initSettings(){
        settingsBackground.setOriginY(0);
        settingsBackground.setScaleY(0);
        settingsBackground.fadeOut();


        muted.fadeOut();
        unmuted.fadeOut();

        if ( AudioModel.getInstance().getSoundOn() ){
            this.muted.hide();
            this.unmuted.show();
        }
        else{
            this.muted.show();
            this.unmuted.hide();
        }
    }


    void toggleSettings(){
        if (settingsCollapsed)
            showSettings();
        else
            collapseSettings();
    }

    private void showSettings(){
        if (!settingsCollapsed)
            return;

        settings.addAction(
                sequence(
                        moveBy(PPM(-4), 0, 0.01f),
                        forever(
                                sequence(
                                        moveBy(PPM(8), 0, 0.02f),
                                        moveBy(PPM(-8), 0, 0.02f)
                                )
                        )
                )

        );

        settingsBackground.addAction(
                parallel(
                        fadeIn(0.5f),
                        scaleTo(1, 1, 0.5f, Interpolation.pow5Out)
                )
        );

        muted.addAction(
                parallel(
                        fadeIn(0.3f, Interpolation.pow5Out),
                        moveBy( 0, PPM(130), 0.3f, Interpolation.pow5Out)
                )
        );
        unmuted.addAction(
                parallel(
                        fadeIn(0.3f, Interpolation.pow5Out),
                        moveBy(0, PPM(130), 0.3f, Interpolation.pow5Out)
                )
        );

        settingsCollapsed = false;

    }

    void collapseSettings(){
        if (settingsCollapsed)
            return;

        settings.clearActions();
        settings.setX(PPM(100));

        settingsBackground.addAction(
                parallel(
                        fadeOut(0.4f),
                        scaleTo(0f, 0, 0.5f, Interpolation.pow5Out)
                )
        );
        muted.addAction(
                parallel(
                        fadeOut(0.3f, Interpolation.pow5Out),
                        moveBy(0, -PPM(130), 0.3f, Interpolation.pow5Out)
                ));

        unmuted.addAction(
                parallel(
                        fadeOut(0.3f, Interpolation.pow5Out),
                        moveBy( 0, -PPM(130) , 0.3f, Interpolation.pow5Out)
                ));

        settingsCollapsed = true;
    }


    //todo: refactor this method to work without a parameter
    void toggleMute(boolean mutedButtonPressed){

        final float animationDelay = 0.1f;
        final MyImage imgToShow, imgToHide;

        if (!mutedButtonPressed){ //aka mute!
            imgToShow = muted;
            imgToHide = unmuted;
            AudioModel.getInstance().soundOff();
        }
        else {
            imgToShow = unmuted;
            imgToHide = muted;
            AudioModel.getInstance().soundOn();
        }

        imgToHide.addAction(
                sequence(
                        fadeOut(animationDelay),
                        hide(),
                        fadeIn(0),
                        run(new Runnable() {
                            @Override
                            public void run() {
                                imgToShow.addAction(
                                        sequence(
                                                fadeOut(0),
                                                show(),
                                                fadeIn( animationDelay)
                                        )
                                );
                            }
                        })
                )
        );
    }
}
