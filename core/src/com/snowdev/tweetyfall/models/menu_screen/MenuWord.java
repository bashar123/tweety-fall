package com.snowdev.tweetyfall.models.menu_screen;


import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.snowdev.tweetyfall.controllers.GameController;
import com.snowdev.tweetyfall.models.shared.AbstractWorld;
import com.snowdev.tweetyfall.models.shared.BirdBodyUserData;
import com.snowdev.tweetyfall.models.shared.misc.*;


import static com.snowdev.tweetyfall.shared.Constants.ColorE;
import static com.snowdev.tweetyfall.shared.Constants.CAMERA_WIDTH_METER;
import static com.snowdev.tweetyfall.shared.Constants.MENU_HORIZONTAL_ACC_SPEED;
import static com.snowdev.tweetyfall.shared.Constants.WAIT_TIME_TO_CREATE_NEW_BIRD_MENU;
import static com.snowdev.tweetyfall.shared.Constants.WORLD_GRAVITY;

public class MenuWord extends AbstractWorld {


    @Override
    protected void createGroundAbstract() {
        BodyFactory.createMenuGroundBody(world);
    }

    @Override
    protected float getWaitTimeToCreateNewBirdAbstract() {
        return WAIT_TIME_TO_CREATE_NEW_BIRD_MENU;
    }

    @Override
    protected void updateWorldObjectAbstract(float mousePos, ColorE nestState) {
        tiltBirdsByAcceleration(GameController.getAcceleration());
        removeBirdsOutsideOfScreen();
    }

    private void tiltBirdsByAcceleration(Vector2 acc){
            float horizontalAcc = - acc.x * MENU_HORIZONTAL_ACC_SPEED;
            Vector2 worldAcceleration = new Vector2(horizontalAcc, WORLD_GRAVITY) ;
            world.setGravity(worldAcceleration);
    }


    private void removeBirdsOutsideOfScreen() {
        if (birdBodies == null || birdBodies.size <= 0){
            return;
        }

        float birdWidth = ((BirdBodyUserData) birdBodies.first().getUserData()).getWidth();

        for(Body bird : birdBodies){
            if((bird.getPosition().x - birdWidth ) > CAMERA_WIDTH_METER || (bird.getPosition().x + birdWidth < 0 )){
                world.destroyBody(bird);
                birdBodies.removeValue(bird, false);
            }
        }
    }

}
