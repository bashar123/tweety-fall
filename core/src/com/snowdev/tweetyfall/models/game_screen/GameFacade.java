package com.snowdev.tweetyfall.models.game_screen;


import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.snowdev.tweetyfall.TweetyFallGame;
import com.snowdev.tweetyfall.models.game_screen.world.GameWord;
import com.snowdev.tweetyfall.models.shared.models.*;
import com.snowdev.tweetyfall.models.game_screen.gfx.AnimationModel;
import com.snowdev.tweetyfall.models.game_screen.gfx.GameGFX;
import com.snowdev.tweetyfall.shared.Utils;

import box2dLight.RayHandler;

import static com.snowdev.tweetyfall.shared.Constants.*;

/**
 * Info
 * ----
 *
 */
public class GameFacade {

    private TweetyFallGame game;

    private AudioModel audioModel;
    private GameGFX gfxModel;
    private GameWord gameWorld;
    private ScoreModel scoreModel;
    private AnimationModel animationModel;
    private boolean isPaused;
    private float nestPos; //todo : hmmm should this be here?


    public GameFacade(TweetyFallGame game) {
        this.game = game;

        gameWorld           = new GameWord();
        gfxModel            = new GameGFX();
        animationModel      = new AnimationModel(gfxModel);

        audioModel          = AudioModel.getInstance();
        scoreModel          = new ScoreModel();
    }


    public void update(float delta) {

        animationModel.updateEnvironment(delta);

        if(!isPaused /*&& counter.isFinished()*/){
            gameWorld.update(delta, nestPos, animationModel.currentNestState);


            if(gameWorld.collisionDetected()) {
                scoreModel.incrementScore(gameWorld.matchState);
                animationModel.birdCached(gameWorld.matchState);
            }

            gfxModel.slideImagesBasedOnNestBodyPosition(gameWorld.nestBodies.first());

            animationModel.updateBirdImageToBirdBodyPos( gameWorld.birdBodies );
            animationModel.updateNextNestAnimation();
            animationModel.updateCachedAnimation();
            animationModel.shrinkInCachedBird(gameWorld.nestBodies.get(1).getPosition(), gameWorld.removedBirdUserData);
            animationModel.updateMatchAnimation();

            checkIfGameOver();
        }
    }


    private void checkIfGameOver(){
        if(DISABLE_GAMEOVER)
            return;

        if(gameWorld.end){
            scoreModel.applyHighScore();
//          audioModel.stopMusic();
            game.setCurrentScreen(ScreenE.GAMEOVER_SCREEN);
        }
    }

    //region ---------------------------------------------------------- BOX2D world -------------------------------------------------------//
    /**
     * To render the Box2dLights
     * @return RayHandler
     */
    public RayHandler getRayHandler(){
        return gameWorld.lightModel.rayHandler;
    }

    /**
     * To render the Box2d lines, debugger
     * @return Box2DDebugRenderer
     */
    public Box2DDebugRenderer getBox2DDebugRenderer(){
        return gameWorld.debugRenderer;
    }

    public World getWorld(){
        return gameWorld.world;
    }
    //endregion

    public void toggleBox2dDebugBorders(){
        DEBUG_RENDER = !DEBUG_RENDER;
    }

    //region ---------------------------------------------------------- grafix -------------------------------------------------------//
    public Array<Actor> getAllGameActors(){
        Array<Actor> images = new Array<>();

        //the order matters
        images.addAll(
                gfxModel.background,
                gfxModel.horizon,
                gfxModel.mountains,


                gfxModel.blueNest,
                gfxModel.redNest,
                gfxModel.purpleToBlueNestAnimatedImage,
                gfxModel.blueToYellowNestAnimatedImage,
                gfxModel.blueBird,
                gfxModel.redBird,
                gfxModel.ground
        );

        images.addAll(gfxModel.stars);

        images.add(gfxModel.moonImage);

        images.addAll(gfxModel.redBirdAnimatedImagePool);
        images.addAll(gfxModel.blueBirdAnimatedImagePool);

        images.addAll(gfxModel.upperCloudArray);
        images.addAll(gfxModel.lowerCloudArray);


        return images;
    }

//    public Array<Actor> getAllGameUiActors(){
//        Array<Actor> ui = new Array<>();
//
//        ui.add(gfxModel.countDownGroup);
//
//        return ui;
//    }
    //endregion

    //region ---------------------------------------------------------- controller -------------------------------------------------------//
    /**
     * Called by the GameController
     * to change the cloud color state, for the animation to change
     */
    public void nextNestColor(){
        if(!isPaused){
            animationModel.nextNestColorState();
        }
    }

    /**
     Called by the controller
     to pause game when user clicks a button(see 'updateClimate' method)
     */
    public void togglePaused(){
        isPaused = !isPaused;
    }


    /**
     Called by the controller
     to updateClimate position (of the nest) by mouse or touch,
     nestPos is sent as a parameter to GameWord (see 'updateClimate' method)
     */
    public void updatePos(Vector2 newPos, boolean byHover){
        if(!isPaused)
            nestPos = newPos.x * LWJGL_APPLICATION_SCALE;
    }

    /**
     * Called by the GameController
     * to go back to menu screen
     */
    public void startScreen(ScreenE screen){
        game.setCurrentScreen(screen);
    }

    public void changeZoom(float zoom){
        Utils.changeZoom(game.cam, zoom);
    }
//endregion

    //region ---------------------------------------------------------- audio -------------------------------------------------------//
    public void playMusic(){
        audioModel.playMusic(0.5f);
    }

    public void pauseMusic(){
        audioModel.pauseMusic();
    }

    public void playPointSound(){
        audioModel.playPointSound();
    }

    public void playGameOverSound(){
        audioModel.playGameOverSound();
    }

    //endregion


    //todo: memory improvements
    public void dispose(){
        gameWorld.dispose();
        gfxModel.dispose();
        audioModel.dispose();
        animationModel.dispose();

//        lightModel.dispose();     //TODO later
    }

}
