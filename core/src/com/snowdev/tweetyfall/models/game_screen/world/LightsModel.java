package com.snowdev.tweetyfall.models.game_screen.world;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.snowdev.tweetyfall.shared.Constants;
import com.snowdev.tweetyfall.shared.Utils;

import box2dLight.PointLight;
import box2dLight.*;


public class LightsModel {
    public RayHandler rayHandler;

    public LightsModel(World world){
        rayHandler = new RayHandler(world);
        rayHandler.setAmbientLight(0.5f);

//        createSun();
    }

    private void createSun(){
        PointLight pointLight = new PointLight(rayHandler,500);
//        pointLight.attachToBody(body);
        pointLight.setColor(1, 1, 1, 0.8f);
//        pointLight.setColor(Color.WHITE);
        pointLight.setDistance(Utils.PPM(1100));
//        pointLight.setXray(true);
        pointLight.setPosition(0, Utils.PPM(Constants.CAMERA_HEIGHT));
        pointLight.update();
    }

    public RayHandler getRayHandler(){
        return rayHandler;
    }

    public void createLight(Body body, int distance){
        PointLight pointLight = new PointLight(rayHandler,500);
        pointLight.attachToBody(body);
        pointLight.setColor(1, 1, 1, 0.8f);
//        pointLight.setColor(Color.WHITE);
        pointLight.setDistance(Utils.PPM(distance));
        pointLight.setXray(true);
        pointLight.setPosition(0, Utils.PPM(Constants.CAMERA_HEIGHT - 100));
        pointLight.update();

    }


}
