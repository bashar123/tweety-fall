package com.snowdev.tweetyfall.models.game_screen.world;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Array;
import com.snowdev.tweetyfall.controllers.CollisionListener;
import com.snowdev.tweetyfall.controllers.GameController;
import com.snowdev.tweetyfall.models.shared.*;
import com.snowdev.tweetyfall.models.shared.misc.*;
import com.snowdev.tweetyfall.shared.Utils;

import static com.snowdev.tweetyfall.shared.Constants.*;
import static com.snowdev.tweetyfall.shared.Constants.ColorE.*;
import static com.snowdev.tweetyfall.shared.Constants.MatchStateE.BLUE_BIRD_MATCH;
import static com.snowdev.tweetyfall.shared.Constants.MatchStateE.BLUE_BIRD_MISMATCH;
import static com.snowdev.tweetyfall.shared.Constants.MatchStateE.RED_BIRD_MATCH;
import static com.snowdev.tweetyfall.shared.Constants.MatchStateE.RED_BIRD_MISMATCH;


public class GameWord extends AbstractWorld {

    public Array<Body> nestBodies;
    public boolean end;
    private CollisionListener collisionListener;

    public BirdBodyUserData removedBirdUserData;

    public MatchStateE matchState;


    public GameWord(){
        nestBodies = new Array<>();
        world.setContactListener(collisionListener = new CollisionListener());

        BodyFactory.createNestBodies(world, nestBodies);
    }


    @Override
    protected void updateWorldObjectAbstract(float mousePos, ColorE nestState) {
        if(DESKTOP_VERSION)
            moveNestByMouse(mousePos);
        else
            moveNestByAcceleration(GameController.getAcceleration().x);

        testIfCollisionOccurred(nestState);
    }

    @Override
    protected void createGroundAbstract() {
        BodyFactory.createGroundBodiesToWorld(world);
    }

    @Override
    protected float getWaitTimeToCreateNewBirdAbstract() {
        return WAIT_TIME_TO_CREATE_NEW_BIRD_GAME;
    }


    //TODO : add collision with walls
    private void moveNestByMouse(float mousePos){
        if(mousePos > 0) {
            float diff = Utils.PPM(mousePos) - nestBodies.get(0).getPosition().x - ((Rectangle) nestBodies.get(0).getUserData()).width * 2;

            nestBodies.get(0).setTransform(nestBodies.get(0).getPosition().x + diff, nestBodies.get(0).getPosition().y, 0);
            nestBodies.get(1).setTransform(nestBodies.get(1).getPosition().x + diff, nestBodies.get(1).getPosition().y, 0);
            nestBodies.get(2).setTransform(nestBodies.get(2).getPosition().x + diff, nestBodies.get(2).getPosition().y, 0);
        }
    }

    //TODO : cleaner code
    private void moveNestByAcceleration(float acc){
        if (acc > -0.1 && acc < 0.1){
            acc = 0;
        }

        //check if the left part of the cloud is in the left corner - if so, place the cloud in the corner
        if(nestBodies.get(0).getPosition().x - ((Rectangle) nestBodies.get(0).getUserData()).width  <=0 && acc>0){
            nestBodies.get(0).setTransform(((Rectangle) nestBodies.get(0).getUserData()).width, nestBodies.get(0).getPosition().y, 0);
            nestBodies.get(1).setTransform(nestBodies.get(0).getPosition().x + ((Rectangle) nestBodies.get(0).getUserData()).width + ((Rectangle) nestBodies.get(1).getUserData()).width, nestBodies.get(1).getPosition().y, 0);
            nestBodies.get(2).setTransform(nestBodies.get(1).getPosition().x + ((Rectangle) nestBodies.get(1).getUserData()).width + ((Rectangle) nestBodies.get(2).getUserData()).width, nestBodies.get(2).getPosition().y, 0);

            filter(-0.2f);
        }

        //check if the right part of the cloud is in the right corner - if so, place the cloud in the corner
        else if (nestBodies.get(2).getPosition().x + ((Rectangle) nestBodies.get(2).getUserData()).width >= CAMERA_WIDTH_METER && acc<0){
            nestBodies.get(2).setTransform(CAMERA_WIDTH_METER - ((Rectangle) nestBodies.get(2).getUserData()).width, nestBodies.get(2).getPosition().y, 0);
            nestBodies.get(1).setTransform(nestBodies.get(2).getPosition().x - ((Rectangle) nestBodies.get(2).getUserData()).width - ((Rectangle) nestBodies.get(1).getUserData()).width , nestBodies.get(1).getPosition().y, 0);
            nestBodies.get(0).setTransform(nestBodies.get(1).getPosition().x - ((Rectangle) nestBodies.get(1).getUserData()).width - ((Rectangle) nestBodies.get(0).getUserData()).width , nestBodies.get(0).getPosition().y, 0);

            filter(0.2f);
        }

        else{
            acc = filter(acc);

            for(Body cloudBody: nestBodies){
                cloudBody.setTransform(cloudBody.getPosition().x - (acc * NEST_ACCELERATION_SPEED), cloudBody.getPosition().y, 0);
            }
        }
    }


    private float xGravity = 0.0f;
    private float filter(float acc){
        float x = acc;
        xGravity = FILTER_ALPHA * xGravity + (1 - FILTER_ALPHA) * x;
        x = xGravity;
        return x;
    }

    private void testIfCollisionOccurred(ColorE nestState){
        //If we have a collision
        if(collisionListener.collidedBirdColor != null){
            if(collisionListener.collisionWithGround)
                end = true;

            else {
                checkBirdNestMatch(nestState);
                removeCollidedBird();
            }

            collisionListener.collidedBirdColor = null;
        }
    }


    private void checkBirdNestMatch(ColorE currentNestColor){
        if(collisionListener.collidedBirdColor == BLUE_BIRD){
            //if match
            if(currentNestColor == BLUE_NEST || currentNestColor == BLUE_NEST_ANIMATING){
                this.matchState = BLUE_BIRD_MATCH;
            }
            else {
                this.matchState = BLUE_BIRD_MISMATCH;
            }
        }
        else if (collisionListener.collidedBirdColor == RED_BIRD){
            //if match
            if(currentNestColor == RED_NEST || currentNestColor == RED_NEST_ANIMATING){
                this.matchState = RED_BIRD_MATCH;
            }
            else {
                this.matchState = RED_BIRD_MISMATCH;
            }
        }
    }

    public boolean collisionDetected() {
        MatchStateE tempMatchState = matchState;
        matchState = null;
        return tempMatchState != null;
    }

    private void removeCollidedBird() {
        removedBirdUserData = (BirdBodyUserData) birdBodies.get(0).getUserData();
        removedBirdUserData.setY(birdBodies.get(0).getPosition().y);

        world.destroyBody(birdBodies.get(0));
        birdBodies.removeIndex(0);
    }


}
