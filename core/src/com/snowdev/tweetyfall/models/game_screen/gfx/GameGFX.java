package com.snowdev.tweetyfall.models.game_screen.gfx;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Array;
import com.snowdev.tweetyfall.models.loading_screen.AssetsHandler;
import com.snowdev.tweetyfall.models.shared.extensions.MyAnimatedImage;
import com.snowdev.tweetyfall.models.shared.extensions.MyImage;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;
import static com.snowdev.tweetyfall.shared.Constants.*;
import static com.snowdev.tweetyfall.shared.Constants.PositionXE.*;
import static com.snowdev.tweetyfall.shared.Constants.PositionYE.*;
import static com.snowdev.tweetyfall.shared.Utils.PPM;


public class GameGFX {

    public MyImage ground, background, moonImage, blueNest, redNest, yellowNest, horizon, mountains, blueBird, redBird;

    public Array<MyAnimatedImage> blueBirdAnimatedImagePool, redBirdAnimatedImagePool;
    public MyAnimatedImage blueToYellowNestAnimatedImage, purpleToBlueNestAnimatedImage, yellowToPurpleNestAnimatedImage; //public: gets called from Facade
    public Array<MyImage> upperCloudArray, lowerCloudArray, stars;

    private float oldNestPos;
    private Array<TextureAtlas> atlases;



    public GameGFX(){
        initiateImages();
        playIntro();
    }



    private void initiateImages(){
        horizon         = AssetsHandler.retrieveImage(AssetsHandler.GAME_HORIZON, true, CENTER_X, VALUE_Y, PPM(50), 0);
        horizon.fadeOut();

        mountains       = AssetsHandler.retrieveImage(AssetsHandler.GAME_MOUNTAINS, true, CENTER_X, VALUE_Y, 0, 0);

        background      = AssetsHandler.retrieveImage(AssetsHandler.GAME_BACKGROUND, true, CENTER_X, VALUE_Y, 0, 0);
        ground          = AssetsHandler.retrieveImage(AssetsHandler.GAME_GROUND, true, CENTER_X,  VALUE_Y, 0, 0);
        ground.setY( -ground.getHeight());

        moonImage       = AssetsHandler.retrieveImage(AssetsHandler.GAME_MOON, true, VALUE_X, VALUE_Y, PPM(-650), 0); //todo: and change the pos to be dynamic (pic image using photoshop) (-moonImage.getWidth())

        blueNest        = AssetsHandler.retrieveImage(AssetsHandler.GAME_NEST_BLUE, true, RIGHT, VALUE_Y, 0, ground.getHeight() - PPM(150));
        redNest         = AssetsHandler.retrieveImage(AssetsHandler.GAME_NEST_PURPLE, true, RIGHT, VALUE_Y, 0, ground.getHeight() - PPM(150));
        yellowNest      = AssetsHandler.retrieveImage(AssetsHandler.GAME_NEST_YELLOW, true, RIGHT, VALUE_Y, 0, ground.getHeight() - PPM(150));

        blueBird        = AssetsHandler.retrieveImage(AssetsHandler.GAME_BLUE_BIRD, true, VALUE_X, VALUE_Y, -1, -1);
        redBird         = AssetsHandler.retrieveImage(AssetsHandler.GAME_RED_BIRD, true, VALUE_X, VALUE_Y, -1, -1);

        upperCloudArray = new Array<>(); //todo use createImages()
        upperCloudArray.add(AssetsHandler.retrieveImage(AssetsHandler.GAME_UPPER_CLOUDS_1, true, VALUE_X, VALUE_Y, 0, PPM(250)));
        upperCloudArray.add(AssetsHandler.retrieveImage(AssetsHandler.GAME_UPPER_CLOUDS_2, true, VALUE_X, VALUE_Y ,upperCloudArray.get(0).getWidth() + upperCloudArray.get(0).getX(), 0));
        upperCloudArray.add(AssetsHandler.retrieveImage(AssetsHandler.GAME_UPPER_CLOUDS_3, true, VALUE_X, VALUE_Y, upperCloudArray.get(1).getWidth() + upperCloudArray.get(1).getX(), 0));
        upperCloudArray.add(AssetsHandler.retrieveImage(AssetsHandler.GAME_UPPER_CLOUDS_4, true, VALUE_X, VALUE_Y, upperCloudArray.get(2).getWidth() + upperCloudArray.get(2).getX(), 0));

        lowerCloudArray = new Array<>(); //todo use createImages()
        lowerCloudArray.add(AssetsHandler.retrieveImage(AssetsHandler.GAME_LOWER_CLOUDS_1, true, VALUE_X, VALUE_Y, 0, PPM(400)));
        lowerCloudArray.add(AssetsHandler.retrieveImage(AssetsHandler.GAME_LOWER_CLOUDS_2, true, VALUE_X, VALUE_Y, lowerCloudArray.get(0).getWidth() + lowerCloudArray.get(0).getX(), 0));
        lowerCloudArray.add(AssetsHandler.retrieveImage(AssetsHandler.GAME_LOWER_CLOUDS_3, true, VALUE_X, VALUE_Y, lowerCloudArray.get(1).getWidth() + lowerCloudArray.get(1).getX(), 0));
        lowerCloudArray.add(AssetsHandler.retrieveImage(AssetsHandler.GAME_LOWER_CLOUDS_4, true, VALUE_X, VALUE_Y, lowerCloudArray.get(2).getWidth() + lowerCloudArray.get(2).getX(), 0));

        initiateStars();
        initiateAnimatedImages();

        oldNestPos = CAMERA_WIDTH_METER / 2f - blueNest.getWidth() / 2f;

    }

    //todo make it more dynamic
    private void initiateStars(){
        stars = new Array<>();

        MyImage newStar ;
        float xPos, yPos, width, height;

        for (int i = 0; i<NUMBER_OF_STARS; i++){
            xPos = MathUtils.random(PPM(-100), CAMERA_WIDTH_METER + PPM(100));

            if(i < 30){ //small stars
                width = height = MathUtils.random(PPM(15), PPM(30));
                yPos = MathUtils.random(CAMERA_HEIGHT_METER/1.7f, CAMERA_HEIGHT_METER);
            }

            else if(i < 48){ //medium stars
                width = height = MathUtils.random(PPM(30), PPM(50));
                yPos = MathUtils.random(CAMERA_HEIGHT_METER/1.4f, CAMERA_HEIGHT_METER);
            }

            else { //large stars
                width = height = MathUtils.random(PPM(50), PPM(70)); //100 is the length of width/height
                yPos = MathUtils.random(CAMERA_HEIGHT_METER/1.2f, CAMERA_HEIGHT_METER);
            }

            newStar = AssetsHandler.retrieveImage(AssetsHandler.GAME_STAR, false);
            newStar.setPosition(xPos, yPos);
            newStar.setSize(width,height);
            newStar.fadeOut();
            stars.add(newStar);
        }
    }

    private void initiateAnimatedImages(){

        atlases = new Array<>(6);
        atlases.addAll(
                AssetsHandler.getAsset(AssetsHandler.GAME_ATLAS_NEST_PURPLE_BLUE, TextureAtlas.class),
                AssetsHandler.getAsset(AssetsHandler.GAME_ATLAS_NEST_BLUE_YELLOW, TextureAtlas.class),
                AssetsHandler.getAsset(AssetsHandler.GAME_ATLAS_NEST_YELLOW_PURPLE, TextureAtlas.class),

                AssetsHandler.getAsset(AssetsHandler.GAME_BLUE_BIRD_ATLAS, TextureAtlas.class),
                AssetsHandler.getAsset(AssetsHandler.GAME_RED_BIRD_ATLAS, TextureAtlas.class)
        );

        Animation animation;

        //Nest animation
        animation = new Animation(NEST_ANIMATION_SPEED * 2,atlases.get(0).getRegions());
        purpleToBlueNestAnimatedImage = new MyAnimatedImage(animation,false,true);

        animation =  new Animation(NEST_ANIMATION_SPEED * 2,atlases.get(1).getRegions());
        blueToYellowNestAnimatedImage = new MyAnimatedImage(animation,false,true);

        animation =  new Animation(NEST_ANIMATION_SPEED * 2,atlases.get(2).getRegions());
        yellowToPurpleNestAnimatedImage = new MyAnimatedImage(animation,false,true);

        //Birds flapping animation loop (MyAnimatedImage)
        Animation blueBirdAnimation = new Animation(BIRD_ANIMATION_SPEED,atlases.get(3).getRegions());
        Animation redBirdAnimation =  new Animation(BIRD_ANIMATION_SPEED,atlases.get(4).getRegions());

        blueBirdAnimatedImagePool = new Array<>();
        redBirdAnimatedImagePool = new Array<>();
        for(int i = 0; i<100; i++){
            redBirdAnimatedImagePool.add (new MyAnimatedImage(redBirdAnimation ,true,true));
            redBirdAnimatedImagePool.get(redBirdAnimatedImagePool.size-1).setPosition(-1,-1);
            blueBirdAnimatedImagePool.add(new MyAnimatedImage(blueBirdAnimation,true,true));
            blueBirdAnimatedImagePool.get(blueBirdAnimatedImagePool.size-1).setPosition(-1,-1);
        }

    }


    //animates: ground, background, moonImage, blueNest, redNest, mountains;
    private void playIntro(){
        ground.addAction(
                moveTo(ground.getX(), PPM(-150), GAMESCREEN_SLIDE_SPEED, GAME_ANIMATION_INTERP)
        );

        moonImage.addAction(
                        moveTo(0, moonImage.getY(), GAMESCREEN_SLIDE_SPEED, GAME_ANIMATION_INTERP)
        );

        blueNest.addAction(
                sequence(
                        scaleBy(-0.5f,-0.5f),
                        scaleBy(0.5f,0.5f, GAMESCREEN_SLIDE_SPEED, GAME_ANIMATION_INTERP)
                )
        );

        redNest.addAction(
                sequence(
                        scaleBy(-0.8f,-0.8f),
                        scaleBy(0.8f,0.8f, GAMESCREEN_SLIDE_SPEED, GAME_ANIMATION_INTERP)
                )
        );

        upperCloudArray.first().addAction(
                moveBy(0, PPM(-250), GAMESCREEN_SLIDE_SPEED, GAME_ANIMATION_INTERP)
        );

        lowerCloudArray.first().addAction(
                moveBy(0, PPM(-400), GAMESCREEN_SLIDE_SPEED, GAME_ANIMATION_INTERP)
        );

        horizon.addAction( alpha(0.5f,2) );

    }


    /**
     * Moves the images (left - right) based on the new position of the nestBody.
     * Images: nest, nest animations, background, horizon, moon, stars, ground.
     */
    //todo :  move to animation model
    public void slideImagesBasedOnNestBodyPosition(Body nestBody){
        //move the nest and its animations to the new position of  the nestBody
        blueNest.setX(nestBody.getPosition().x - ((Rectangle) nestBody.getUserData()).width);
        redNest.setX(nestBody.getPosition().x - ((Rectangle) nestBody.getUserData()).width);

        purpleToBlueNestAnimatedImage.setPosition(nestBody.getPosition().x - PPM(39), nestBody.getPosition().y - PPM(152)); //TODO "hardcoded"
        blueToYellowNestAnimatedImage.setPosition(nestBody.getPosition().x - PPM(39), nestBody.getPosition().y - PPM(152)); //TODO "hardcoded"

        float newNestPos = blueNest.getX();
        float difference = newNestPos - oldNestPos;

        //moves the background new pos to the opposite dir
        ground.setX(ground.getX() - difference/10f);
        mountains.setX(mountains.getX() + difference/15f);
        moonImage.setX(moonImage.getX() + difference/30f);


        //MOVE THE STARS
        float d ;
        for(MyImage star : stars){
            if(star.getWidth()>= PPM(15) && star.getWidth()<= PPM(25) )
                d = difference/17;

            else if (star.getWidth()>= PPM(25) && star.getWidth()<= PPM(40))
                d = difference/16;

            else
                d = difference/15;

            star.setX(star.getX() + d);
        }

        oldNestPos = newNestPos;
    }





    //todo: memory improvements
    public void dispose() {
        for (TextureAtlas atlas : atlases) {
            atlas.dispose();
        }
    }

}