package com.snowdev.tweetyfall.models.game_screen.gfx;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Array;
import com.snowdev.tweetyfall.models.shared.*;
import com.snowdev.tweetyfall.models.shared.extensions.MyAnimatedImage;
import com.snowdev.tweetyfall.models.shared.extensions.MyImage;
import com.snowdev.tweetyfall.shared.Utils;

import static com.snowdev.tweetyfall.shared.Constants.*;
import static com.snowdev.tweetyfall.shared.Constants.ColorE.*;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;



public class AnimationModel {

    private GameGFX gfxModel;
    
    private float starEffectTimePassed = 0;
    private boolean birdCached;
    private boolean nestAnimationInProgress;

//    private boolean match;
//    private ColorE cachedBirdColor;

    public ColorE currentNestState;


    public AnimationModel(GameGFX gfxModel){
        this.gfxModel = gfxModel;

        chooseRandomNestColor();
    }


    private void chooseRandomNestColor(){

        if(MathUtils.randomBoolean()){
            currentNestState = BLUE_NEST;
            gfxModel.redNest.setVisible(false);
            gfxModel.blueNest.setVisible(true);
        }
        else {
            currentNestState = RED_NEST;
            gfxModel.redNest.setVisible(true);
            gfxModel.blueNest.setVisible(false);
        }
    }



    // ----------------------------------- change states -------------------------------------------
    public void birdCached(MatchStateE matchState){
        birdCached = true;
//        switch (matchState) {
//            case RED_BIRD_MATCH:
//                cachedBirdColor = RED_BIRD;
//                match = true;
//                break;
//            case BLUE_BIRD_MATCH:
//                cachedBirdColor = BLUE_BIRD;
//                match = true;
//                break;
//            case RED_BIRD_MISMATCH:
//                cachedBirdColor = RED_BIRD;
//                match = false;
//                break;
//            case BLUE_BIRD_MISMATCH:
//                cachedBirdColor = BLUE_BIRD;
//                match = false;
//                break;
//        }
    }

    public void nextNestColorState(){
        nestAnimationInProgress = false;

        if(currentNestState == BLUE_NEST){
            currentNestState = RED_NEST_ANIMATING;
        }
        else if(currentNestState == RED_NEST_ANIMATING){
            currentNestState = BLUE_NEST_ANIMATING;

        }
        else if(currentNestState == RED_NEST){
            currentNestState = BLUE_NEST_ANIMATING;
        }
        else if(currentNestState == BLUE_NEST_ANIMATING){
            currentNestState = RED_NEST_ANIMATING;
        }
    }

//    public void nextNestColorState2(){
//        nestAnimationInProgress = false;
//
////        (b) -> br -> (r) -> ry -> (y) -> yb ->
////
////        if( b or yb)
////          br
////        if(r or br)
////          ry
////        if(y or ry)
////          yb
//
//        if(currentNestState == BLUE_NEST || currentNestState == YELLOW_BLUE_NEST ){
//            currentNestState = BLUE_RED_NEST;
//        }
//        else if(currentNestState == RED_NEST || currentNestState == BLUE_RED_NEST){
//            currentNestState = RED_YELLOW_NEST;
//        }
//        else if(currentNestState == YELLOW || currentNestState == RED_YELLOW_NEST){
//            currentNestState = YELLOW_BLUE;
//        }
//
//    }




    // ---------------------------------- main animation ------------------------------------------
    /**
     *Environment, keeps on looping stars and clouds even if game is paused
     */
    public void updateEnvironment(float delta){
        updateStarAnimation(delta);
        moveCloudsToLeft(delta);
    }


	/**
     * Nest animation to new color when screen is tapped
     */
    public void updateNextNestAnimation(){
        
        MyImage blueNestImage = gfxModel.blueNest, redNestImage = gfxModel.redNest;
        MyAnimatedImage blueNestAnimatedImage = gfxModel.purpleToBlueNestAnimatedImage, redNestAnimatedImage = gfxModel.blueToYellowNestAnimatedImage;
        
        blueNestImage.setVisible(false);
        redNestImage.setVisible(false);
        
        if (currentNestState == BLUE_NEST_ANIMATING) {
            if(!nestAnimationInProgress){
                blueNestAnimatedImage.startAnimation(false);
                nestAnimationInProgress = true;
            }

            if(blueNestAnimatedImage.isFinished()){
                currentNestState = BLUE_NEST;
                nestAnimationInProgress = false;
            }
        }

        if (currentNestState == BLUE_NEST){
            blueNestImage.setVisible(true);
        }

       if (currentNestState == RED_NEST_ANIMATING) {
            if(!nestAnimationInProgress){
                redNestAnimatedImage.startAnimation(false);
                nestAnimationInProgress = true;
            }

            if(redNestAnimatedImage.isFinished()){
                currentNestState = RED_NEST;
                nestAnimationInProgress = false;
            }
        }

         if (currentNestState == RED_NEST){
            redNestImage.setVisible(true);
        }
    }


    /**
     * Feather animation when bird is cached and a match
     */
    public void updateMatchAnimation( ){
//        MyAnimatedImage blueFeatherAnimatedImage = gfxModel.blueFeatherAnimatedImage, redFeatherAnimatedImage = gfxModel.redFeatherAnimatedImage;
//        if(match) {
//            if(cachedBirdColor == BLUE_BIRD){
//                blueFeatherAnimatedImage.startAnimation(true);
//            }
//            else if(cachedBirdColor ==RED_BIRD){
//                redFeatherAnimatedImage.startAnimation(true);
//            }
//            playFeatherEffect = false;
//        }
    }

    /**
     * Bubble animation when bird is cached
     */
    public void updateCachedAnimation(){
        MyImage blueNestImage = gfxModel.blueNest, redNestImage = gfxModel.redNest;
        MyAnimatedImage blueNestAnimatedImage = gfxModel.purpleToBlueNestAnimatedImage, redNestAnimatedImage = gfxModel.blueToYellowNestAnimatedImage;

        if(birdCached){
            addBubbleAction(blueNestAnimatedImage);
            addBubbleAction(blueNestImage);
            addBubbleAction(redNestAnimatedImage);
            addBubbleAction(redNestImage);
            birdCached = false;
        }
    }

    private void addBubbleAction(MyImage nest){
        nest.setOrigin(nest.getWidth()/2, nest.getHeight()/2);
        nest.addAction(
                sequence(
                        scaleBy(0.2f, 0.2f),
                        scaleBy(-0.2f, -0.2f, 0.5f)
                )
        );
    }

	/**
     * Bird loop animation
     */
    public void updateBirdImageToBirdBodyPos(Array<Body> birds){

        Array<MyAnimatedImage> blueBirdAnimatedImagePool = gfxModel.blueBirdAnimatedImagePool, redBirdAnimatedImagePool = gfxModel.redBirdAnimatedImagePool;

        placeBirdImagesOutOfScreen(blueBirdAnimatedImagePool);
        placeBirdImagesOutOfScreen(redBirdAnimatedImagePool);

        //place an animated bird in every bird body position
        for(int i = 0; i<birds.size; i++){
            Body birdBody = birds.get(i);

            BirdBodyUserData birdUserData = (BirdBodyUserData) birdBody.getUserData();

            if (birdUserData.getColor() == BLUE_BIRD){
                blueBirdAnimatedImagePool.get(i).setPosition(birdBody.getPosition().x - Utils.PPM(100), birdBody.getPosition().y - Utils.PPM(70));
                blueBirdAnimatedImagePool.get(i).setRotation(birdBody.getAngle() * MathUtils.radiansToDegrees);
            }

            else if (birdUserData.getColor() == RED_BIRD){
                redBirdAnimatedImagePool.get(i).setPosition(birdBody.getPosition().x - Utils.PPM(100), birdBody.getPosition().y - Utils.PPM(70));
                redBirdAnimatedImagePool.get(i).setRotation(birdBody.getAngle() * MathUtils.radiansToDegrees);
            }
            else{
                throw new IllegalArgumentException("Unknown bird color: " + birdUserData.getColor());
            }
        }

    }

    private void placeBirdImagesOutOfScreen(Array<MyAnimatedImage> birdPool){
        for(MyImage bird: birdPool){
            bird.setPosition(-1,-1);
        }
    }


    public void shrinkInCachedBird(Vector2 nestPos, BirdBodyUserData birdUserData){

        MyImage blueBird = gfxModel.blueBird, redBird = gfxModel.redBird;

        if(birdUserData == null || !birdUserData.isAlive())
            return;

        Vector2 deltaDistance = new Vector2(nestPos.x - birdUserData.getX(), nestPos.y - birdUserData.getY());
        deltaDistance.x = deltaDistance.x * 0.6f;
        deltaDistance.y = deltaDistance.y * 0.8f;

        MyImage tempImage = birdUserData.getColor() == BLUE_BIRD ? blueBird : redBird ;

        tempImage.resetImage();
        tempImage.setPosition(birdUserData.getX() - Utils.PPM(100), birdUserData.getY() - Utils.PPM(70)); //todo find a solution to 100 and 70

        float scale = 0.4f, delay = 0.2f;
        tempImage.addAction(
                parallel(
                    scaleBy(-scale, -scale, delay, GAME_ANIMATION_INTERP),
                    moveBy(deltaDistance.x, deltaDistance.y, delay, Interpolation.exp5Out)
                    , fadeOut(delay)
                )
        );

        birdUserData.setAlive(false);
    }

    //......................................................climate......................................................................
	/**
     * Animate upper clouds to left, gets called on twice: for upper- and lower clouds
     */
    private void moveCloudsToLeft(float deltaTime){
	    moveCloudsToLeft(gfxModel.upperCloudArray, UPPER_CLOUD_SPEED, deltaTime);
	    moveCloudsToLeft(gfxModel.lowerCloudArray, LOWER_CLOUD_SPEED, deltaTime);
    }

    private void moveCloudsToLeft(Array<MyImage> cloudImages, float speed, float deltaTime){
        MyImage front = getImage(cloudImages, true);
        MyImage last = getImage(cloudImages, false);

        if(front.getX()+front.getWidth()<-1){
            front.setX(last.getWidth() + last.getX());
        }

        for(MyImage i: cloudImages){
            i.setX(i.getX() - (speed * deltaTime));
        }
    }

    //Get the front or last image in array based on X coordinate of the MyImage
    private MyImage getImage(Array<MyImage> arr, boolean inFront){
        MyImage temp = arr.first();

        if(inFront){
            for(MyImage i: arr){
                if(i.getX() < temp.getX())
                    temp = i;
            }
        }
        else { //in last
            for(MyImage i: arr){
                if(i.getX() > temp.getX())
                    temp = i;
            }
        }
        return temp;
    }


    /**
     * fadeout star "animation"
     */
    private void updateStarAnimation(float delta){
        Array<MyImage> stars = gfxModel.stars;

        starEffectTimePassed += delta;

        if(starEffectTimePassed > STAR_ANIMATION_DELAY) {
            MyImage randomStar = stars.get(MathUtils.random(0, stars.size - 1));
            animateStar(randomStar);
            starEffectTimePassed = 0;
        }
    }


    private void animateStar(MyImage randomStar){
        if(randomStar.isDoneAnimating()) {
            randomStar.addAction(
                    sequence(
                            parallel(
                                    scaleBy(0.5f, 0.5f, 0.1f, GAME_ANIMATION_INTERP),
                                    fadeIn(0.1f, GAME_ANIMATION_INTERP)
                            ),
                            scaleBy(-0.5f, -0.5f, 0.5f, GAME_ANIMATION_INTERP),
                            alpha(0.8f, 1f, GAME_ANIMATION_INTERP)
                    )
            );
        }
    }


    public void dispose(){}
}
