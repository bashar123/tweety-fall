package com.snowdev.tweetyfall.models.shared.misc;


import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.snowdev.tweetyfall.models.shared.BirdBodyUserData;
import com.snowdev.tweetyfall.shared.Utils;


import static com.snowdev.tweetyfall.shared.Constants.*;

//TODO: Clean this mess
/**
 * Methods in this class are intended to be called statically
 */
public class BodyFactory {

    /**
     * Creates a bird body and add it to world with color as UserData
     */
    public static Body createBirdBody(float x, float y, ColorE color, World world){
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(x , y);
        bodyDef.allowSleep = false;

        float halfWidth = Utils.PPM(60);
        float halfHeight = Utils.PPM(50);
        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(halfWidth, halfHeight);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = polygonShape;
        fixtureDef.density = 2.5f;
        fixtureDef.friction = 0.25f;
        fixtureDef.restitution = 0.4f;
        fixtureDef.filter.categoryBits = BIT_BIRD;
//        fixtureDef.filter.maskBits = 1; //all but BIT_BIRD

        Body birdBody = world.createBody(bodyDef);
        birdBody.createFixture(fixtureDef);

        BirdBodyUserData bodyUserData = new BirdBodyUserData(true, color, halfWidth * 2, halfHeight * 2, x, y);
        birdBody.setUserData(bodyUserData);

        polygonShape.dispose();
        return birdBody;
    }

    /**
     * Creates ground bodies
     */
    public static void createGroundBodiesToWorld(World world){
        //--------------------------Ground1--------------------------------
        BodyDef groundBodyDef = new BodyDef();
        groundBodyDef.position.set(new Vector2(Utils.PPM(190), Utils.PPM(287)));

        Body groundBody = world.createBody(groundBodyDef);

        PolygonShape groundBox = new PolygonShape();
        groundBox.setAsBox(Utils.PPM(190), Utils.PPM(287));

        groundBody.createFixture(groundBox, 0.0f);
        groundBody.setUserData("ground");
//        lightModel.createLight(groundBody, 500);


        //--------------------------Ground2--------------------------------
        groundBodyDef.position.set(new Vector2(Utils.PPM(190 * 2 + 121), Utils.PPM(251)));

        groundBody = world.createBody(groundBodyDef);

        groundBox.setAsBox(Utils.PPM(120), Utils.PPM(250));

        groundBody.createFixture(groundBox, 0.0f);
        groundBody.setUserData("ground");
//        lightModel.createLight(groundBody, 500);


        //--------------------------Ground3--------------------------------
        groundBodyDef.position.set(new Vector2(Utils.PPM(190 * 2 + 120 * 2 + 115), Utils.PPM(285)));

        groundBody = world.createBody(groundBodyDef);

        groundBox.setAsBox(Utils.PPM(113), Utils.PPM(285));

        groundBody.createFixture(groundBox, 0.0f);
        groundBody.setUserData("ground");
//        lightModel.createLight(groundBody, 500);


        //--------------------------Ground4--------------------------------
        groundBodyDef.position.set(new Vector2(Utils.PPM(190 * 2 + 120 * 2 + 113 * 2 + 123), Utils.PPM(251)));

        groundBody = world.createBody(groundBodyDef);

        groundBox.setAsBox(Utils.PPM(120), Utils.PPM(250));

        groundBody.createFixture(groundBox, 0.0f);
        groundBody.setUserData("ground");
//        lightModel.createLight(groundBody, 500);

        groundBox.dispose();
    }


    /**
     * Creates ground body for menu screen
     */
    public static void createMenuGroundBody(World world){
        //--------------------------Ground1--------------------------------
        BodyDef groundBodyDef = new BodyDef();
        groundBodyDef.position.set(new Vector2(CAMERA_WIDTH_METER/2, Utils.PPM(1)));

        Body groundBody = world.createBody(groundBodyDef);

        PolygonShape groundBox = new PolygonShape();
        groundBox.setAsBox(CAMERA_WIDTH_METER/2, Utils.PPM(1));

        groundBody.createFixture(groundBox, 0.0f);
        groundBody.setUserData("ground");
//        lightModel.createLight(groundBody, 500);


        groundBox.dispose();

    }


    /**
     * Creates nest bodies and add it to the nestBodies array
     */
    public static void createNestBodies(World world, Array<Body> nestBodies){
        //----------------------------KINNEMATICBODY(nest) part1--------------------------
        BodyDef nestBodyDef = new BodyDef();

        nestBodyDef.position.set(new Vector2(Utils.PPM(CAMERA_WIDTH / 2f - 128), Utils.PPM(CAMERA_HEIGHT / 2 - 225)));
        nestBodyDef.type = BodyDef.BodyType.KinematicBody;

        Body nestBody = world.createBody(nestBodyDef);
//        lightModel.createLight(groundBody, 100);


        PolygonShape nestBox = new PolygonShape();
        nestBox.setAsBox(Utils.PPM(40), Utils.PPM(30));
        nestBody.createFixture(nestBox, 0.0f);
        nestBody.setUserData(new Rectangle(0, 0, Utils.PPM(40), Utils.PPM(30))); //Sparar width ch height för senare användning (se changenestPosByAcceleration())
        nestBodies.add(nestBody);

        //----------------------------KINNEMATICBODY(nest) part2--------------------------
        nestBodyDef.position.set(new Vector2(Utils.PPM(CAMERA_WIDTH / 2f - 47), Utils.PPM(CAMERA_HEIGHT / 2 - 220)));
        nestBodyDef.type = BodyDef.BodyType.KinematicBody;

        nestBody = world.createBody(nestBodyDef);
//        lightModel.createLight(groundBody, 100);

        nestBox.setAsBox(Utils.PPM(40), Utils.PPM(45));
        nestBody.createFixture(nestBox, 0.0f);
        nestBody.setUserData(new Rectangle(0, 0, Utils.PPM(40), Utils.PPM(45)));
        nestBodies.add(nestBody);

        //----------------------------KINNEMATICBODY(nest) part3--------------------------
        nestBodyDef.position.set(new Vector2(Utils.PPM(CAMERA_WIDTH / 2f + 55), Utils.PPM(CAMERA_HEIGHT / 2 - 210)));
        nestBodyDef.type = BodyDef.BodyType.KinematicBody;

        nestBody = world.createBody(nestBodyDef);
//        lightModel.createLight(groundBody, 100);

        nestBox.setAsBox(Utils.PPM(60), Utils.PPM(50));
        nestBody.createFixture(nestBox, 0.0f);
        nestBody.setUserData(new Rectangle(0, 0, Utils.PPM(60), Utils.PPM(50)));
        nestBodies.add(nestBody);

        nestBox.dispose();
    }

}
