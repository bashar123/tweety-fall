package com.snowdev.tweetyfall.models.shared;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.snowdev.tweetyfall.models.game_screen.world.LightsModel;
import com.snowdev.tweetyfall.models.shared.misc.BodyFactory;
import com.snowdev.tweetyfall.shared.Utils;

import static com.snowdev.tweetyfall.shared.Constants.*;
import static com.snowdev.tweetyfall.shared.Constants.ColorE.*;


/**
 * 1. creates word (and steps world)
 * 2. creates bird box2d bodies (test to create new bird and bird pos X )
 * 3. creates debug renderer and light model(not finished)
 */
public abstract class AbstractWorld{

    public World world;
    public Array<Body> birdBodies;

    public Box2DDebugRenderer debugRenderer;
    public LightsModel lightModel;


    private float dropBirdTimer = 0, changeBirdColorTimer = 0;
    private final float waitTimeForNewBirdColor = 0.5f; //sec

    private ColorE currentBirdColor = BLUE_BIRD;


    public AbstractWorld(){
        world = new World(new Vector2(0,WORLD_GRAVITY), true);

        debugRenderer = new Box2DDebugRenderer();
        lightModel = new LightsModel(world);

        birdBodies = new Array<>();

        createGroundAbstract();
    }


    protected abstract void createGroundAbstract();
    protected abstract void updateWorldObjectAbstract(float mousePos, ColorE nestState);
    protected abstract float getWaitTimeToCreateNewBirdAbstract();


    public void update(float deltaTime, float mousePos, ColorE nestState){
        createNewBird( deltaTime );
        stepWorld( deltaTime );
        updateWorldObjectAbstract( mousePos, nestState );
    }


    private double accumulator;
    private void stepWorld(float deltaTime){
        float frameTime = Math.min(deltaTime, 0.25f);
        accumulator += frameTime;
        while (accumulator >= TIME_STEP) {
            world.step(TIME_STEP, 6, 2);
            accumulator -= TIME_STEP;
        }
    }

    //todo send screen (ScreenE) instead of deltaTimeToCreateNewBird
    private void createNewBird(float delta){
        dropBirdTimer += delta;
        changeBirdColorTimer += delta;

        float waitTimeToCreateNewBird = getWaitTimeToCreateNewBirdAbstract();

        if(dropBirdTimer >= waitTimeToCreateNewBird){

            Body newBird = BodyFactory.createBirdBody(calculateXPosition() , CAMERA_HEIGHT_METER + Utils.PPM(100), getBirdColor(), world);
            birdBodies.add(newBird);

            dropBirdTimer = 0;
        }
    }



    private ColorE getBirdColor(){
        if( changeBirdColorTimer >= waitTimeForNewBirdColor){
            currentBirdColor = MathUtils.randomBoolean() ? RED_BIRD : BLUE_BIRD; //Try to change bird color
            changeBirdColorTimer = 0;
        }

        return currentBirdColor;
    }



    //TODO: long method
    private final float cellWidth = CAMERA_WIDTH / NR_OF_COLUMNS;
    private int targetColumn = MathUtils.random(1, NR_OF_COLUMNS);
    private int currentColumn = MathUtils.random(1, NR_OF_COLUMNS);
    private float calculateXPosition(){

        //the current pos -> nearer spawnPos ( x2 if random )
        if(currentColumn > targetColumn) {
            currentColumn--;
            if (currentColumn > targetColumn) {
                boolean tempBool = MathUtils.randomBoolean();
                currentColumn = tempBool ? --currentColumn : currentColumn;
            }
        }

        //the current pos -> nearer spawnPos x2 ( x2 if possible )
        else if(currentColumn < targetColumn) {
            currentColumn++;
            if (currentColumn < targetColumn) {
                boolean tempBool = MathUtils.randomBoolean();
                currentColumn = tempBool ? ++currentColumn : currentColumn;
            }
        }

        //randomize a new spawn pos to move to
        if(currentColumn == targetColumn){
            int oldSpawnIndex = targetColumn;

            do {
                targetColumn = MathUtils.random(1, NR_OF_COLUMNS);
            } while(oldSpawnIndex == targetColumn);
        }


        float currentPos = (currentColumn * cellWidth) - (cellWidth/2f);

        //check if the bird is in first column (1)
        if(currentColumn == 1){
            return Utils.PPM(currentPos + 20);
        }
        //check if the bird is in last column
        else if(currentColumn == NR_OF_COLUMNS){
            return Utils.PPM(currentPos - 20);
        }
        else {
            return Utils.PPM(currentPos);
        }
    }

    //todo: memory improvements
    public void dispose(){
        world.dispose();
        debugRenderer.dispose();
    }

}
