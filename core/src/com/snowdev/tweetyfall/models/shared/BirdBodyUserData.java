package com.snowdev.tweetyfall.models.shared;

import static com.snowdev.tweetyfall.shared.Constants.ColorE;


public class BirdBodyUserData {

    private boolean isAlive;

    private ColorE color;

    private float width;
    private float height;

    private float x;
    private float y;

    public BirdBodyUserData(boolean isAlive, ColorE color, float width, float height, float x, float y) {
        this.isAlive = isAlive;
        this.color = color;
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public ColorE getColor() {
        return color;
    }


    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }
}
