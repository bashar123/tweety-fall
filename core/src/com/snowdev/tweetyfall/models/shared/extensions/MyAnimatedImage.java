package com.snowdev.tweetyfall.models.shared.extensions;


import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class MyAnimatedImage extends MyImage
{
    private Animation animation;
    private boolean loop;
    private float animationTime = 0;


    public MyAnimatedImage(Animation animation, boolean loop, boolean convertToMeter) {
        super(animation.getKeyFrame(0), convertToMeter );

        this.animation  = animation;
        this.loop       = loop;

        if(!loop)
            this.setVisible(false);

    }

    @Override
    public void act(float delta)
    {
        if(isFinished() && !loop){
            this.setVisible(false);
        }
        animationTime = animationTime + delta;
        ((TextureRegionDrawable)getDrawable()).setRegion(animation.getKeyFrame(animationTime, loop));

        super.act(delta);
    }

    /**
     * If force = true : animation will play from beginning even though animation may not be finished.
     */
    public void startAnimation(boolean force) {
        if(!loop){
            if(force){
                animationTime = 0;
                this.setVisible(true);
            }

            else{
                if(isFinished()){
                    animationTime = 0;
                    this.setVisible(true);
                }
            }
        }
    }

    public boolean isFinished(){
        return animation.isAnimationFinished(animationTime);
    }

}
