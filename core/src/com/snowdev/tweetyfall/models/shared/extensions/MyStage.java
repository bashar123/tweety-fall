package com.snowdev.tweetyfall.models.shared.extensions;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Fixes/improves a "bug" in the LibGDX framework
 */

public class MyStage extends Stage {

    public MyStage(Viewport viewport){
        super(viewport);
    }

//    public MyStage(Viewport viewport, Batch batch){
//        super(viewport, batch);
//    }


    public boolean finishedAnimating(){

        if(getRoot().getActions().size != 0)
            return false;

        for (Actor actor: getActors() ){
            if (actor.getActions().size != 0){
                return false;
            }
        }

        return true;
    }

    /**
     * A slightly modified 'Draw', tests if batch is already begun, if so then draw the textures directly
     */
    /*
    @Override
    public void draw() {
        Camera camera = getViewport().getCamera();
        camera.update();

        if (!getRoot().isVisible())
            return;

        Batch batch = getBatch();
        batch.setProjectionMatrix(camera.combined);

        //Here lays the fix (test if batch is already begun)
        if(!batch.isDrawing()){
            batch.begin();
            getRoot().draw(batch, 1);
            batch.end();
        }
        else {
            getRoot().draw(batch, 1);
        }

        //debug has a package access....
//        if (debug) drawDebug();
    }
    */
}
