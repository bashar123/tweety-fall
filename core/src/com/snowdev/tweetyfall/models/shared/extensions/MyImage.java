package com.snowdev.tweetyfall.models.shared.extensions;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.snowdev.tweetyfall.shared.Constants.PositionXE;
import com.snowdev.tweetyfall.shared.Constants.PositionYE;

import static com.snowdev.tweetyfall.shared.Constants.CAMERA_HEIGHT;
import static com.snowdev.tweetyfall.shared.Constants.CAMERA_HEIGHT_METER;
import static com.snowdev.tweetyfall.shared.Constants.CAMERA_WIDTH;
import static com.snowdev.tweetyfall.shared.Constants.CAMERA_WIDTH_METER;
import static com.snowdev.tweetyfall.shared.Utils.PPM;


public class MyImage extends Image {

	private boolean inMeter; //final
	private float originalWidth, originalHeight; //final

	// ----------------------------- constructors begin ---------------------------------------

	/**
	 * @param x if param(posX) = CENTER_X, then this value will be the leftMargin
	 * @param y if param(posY) != VALUE_Y, then this param wont be used
	 */
	public MyImage(Texture texture, boolean convertToMeter, PositionXE posX, PositionYE posY, float x, float y){
		super(texture);
		init(convertToMeter);

		switch (posX){
			case CENTER_X:
				centerX(x);
				break;
			case LEFT:
				setX(-getWidth());
				break;
			case RIGHT:
				setX( inMeter ? CAMERA_WIDTH_METER : CAMERA_WIDTH);
				break;
			case VALUE_X:
				setX(x);
				break;
		}

		switch (posY){
			case CENTER_Y:
				centerY();
				break;
			case BOTTOM:
				setY(-getHeight());
				break;
			case TOP:
				setY(inMeter ? CAMERA_HEIGHT_METER : CAMERA_HEIGHT);
				break;
			case VALUE_Y:
				setY(y);
				break;
		}
	}

	public MyImage(Texture texture, boolean convertToMeter){
		super(texture);
		init(convertToMeter);
	}


	//Used by MyAnimatedImage
	MyImage(TextureRegion textureRegion, boolean convertToMeter){
		super(textureRegion);
		init(convertToMeter);
	}

	// ----------------------------- constructors end ---------------------------------------


	private void init(boolean convertToMeter){
		if(convertToMeter) resizeToMeter();

		originalWidth = getWidth();
		originalHeight = getHeight();
		centerOrigin();
	}

	@Override
	public void setSize(float width, float height) {
		super.setSize(width, height);
		centerOrigin();
	}

	private void resizeToMeter(){
		setSize(PPM(getWidth()), PPM(getHeight()));
		inMeter = true;
	}

	public float getCenterX(){
		float cameraWidth = inMeter ? CAMERA_WIDTH_METER : CAMERA_WIDTH;
		return cameraWidth / 2 - getWidth() / 2;
	}

	public float getCenterY(){
		float cameraHeight = inMeter ? CAMERA_HEIGHT_METER : CAMERA_HEIGHT;
		return cameraHeight / 2 - getHeight() / 2;
	}

	public void centerX(){
		centerX(0);
	}

	public void centerX(float leftMargin){
		float cameraWidth = inMeter ? CAMERA_WIDTH_METER : CAMERA_WIDTH;
		setX(cameraWidth / 2 - getWidth() / 2 + leftMargin );
	}

	public void centerY(){
		float cameraHeight = inMeter ? CAMERA_HEIGHT_METER : CAMERA_HEIGHT;
		setY(cameraHeight / 2 - getHeight() / 2 );
	}

	private void centerOrigin(){
		setOrigin(getWidth()/2f, getHeight()/2f);
	}


	public boolean isDoneAnimating(){
		return getActions().size == 0;
	}

	/**
	 * Clears actions and restores original scale, size and alpha
	 */
	public void resetImage(){
		this.clearActions();
		setScale(1,1);
		setSize(originalWidth, originalHeight);
		getColor().a = 1; //alpha
	}


	// ---------------------------------------------- Actions -----------------------------------
	public void hide(){
		addAction(
				Actions.hide()
		);
	}

	public void show(){
		addAction(
				Actions.show()
		);
	}

	public void fadeOut(){
		getColor().a = 0;
	}

	public void fadeIn(){
		getColor().a = 1;
	}

	//-------------------------------------------------------------------------------------------

}
