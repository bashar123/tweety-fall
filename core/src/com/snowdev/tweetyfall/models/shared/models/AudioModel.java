package com.snowdev.tweetyfall.models.shared.models;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.audio.Music;
import com.snowdev.tweetyfall.models.loading_screen.AssetsHandler;


public class AudioModel {
	private Sound pointSound;
    private Sound gameOverSound;
	private Music backgroundMusic;
	private boolean soundOn;


	private static AudioModel audioModel = null;
	public static AudioModel getInstance(){
	    if(audioModel == null)
	        audioModel = new AudioModel();
	    return audioModel;
    }

    private AudioModel(){
        pointSound = AssetsHandler.getAsset(AssetsHandler.POINT_SOUND, Sound.class);
        gameOverSound = AssetsHandler.getAsset(AssetsHandler.LOSE_SOUND, Sound.class);
        backgroundMusic = AssetsHandler.getAsset(AssetsHandler.BACKGROUND_MUSIC, Music.class);
		backgroundMusic.setLooping(true);

		getCurrentAudioState();
    }

    private void getCurrentAudioState(){
        //todo: soundOn = get from file
    }



    public void playMusic(float volume){
        if(soundOn){
            backgroundMusic.setVolume(volume);
            backgroundMusic.play();
        }
    }

    public void pauseMusic(){
		backgroundMusic.pause();
    }

    public void stopMusic(){
        backgroundMusic.stop();
    }

    public void playPointSound(){
        if(soundOn) {
            pointSound.play();
        }
    }

    public void playGameOverSound(){
        if(soundOn){
            gameOverSound.play();
        }
    }

    public void soundOn(){
        soundOn = true;
        //todo: save in file
    }

    public void soundOff(){
        soundOn = false;
        //todo: save in file
    }

    public boolean getSoundOn(){
        return soundOn;
    }

    public void dispose(){
        pointSound.dispose();
        gameOverSound.dispose();
        backgroundMusic.dispose();
    }



}
