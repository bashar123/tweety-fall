package com.snowdev.tweetyfall.models.shared.models;


public interface Model {
    void update();
    void pause();
    void resume();
    void dispose();
}
