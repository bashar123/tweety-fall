package com.snowdev.tweetyfall.models.shared.models;


import com.snowdev.tweetyfall.shared.Constants;

import static com.snowdev.tweetyfall.shared.Constants.MatchStateE.*;
import static com.snowdev.tweetyfall.shared.Constants.MatchStateE;

public class ScoreModel {
    private int currentScore;
    private int highScore;

    public ScoreModel(){
        currentScore = 0;
        highScore = 100; //TODO LÄS FRÅN highscore-FIL, om filen inte finns => skapa den med highscore = 0 :)
    }

    public int getCurrentScore() {
        return currentScore;
    }

    public int getHighScore() {
        return highScore;
    }

    public void incrementScore(MatchStateE matchState){
        if(matchState == RED_BIRD_MATCH || matchState == BLUE_BIRD_MATCH){
            currentScore += 5;
        }
        else{
            currentScore ++;
        }

    }

    public void applyHighScore(){
        if(highScore<currentScore){
            highScore = currentScore;
            //TODO SPARA TILL highscore-FIL
        }
    }

}
