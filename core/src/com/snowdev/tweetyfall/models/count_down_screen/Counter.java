package com.snowdev.tweetyfall.models.count_down_screen;

public class Counter {

    private float remainder;

    Counter(float delay) {
        // Adds 0.2 so it looks like it counts from 3 (counts too fast)
        remainder = delay + 0.2f;
    }

    public float countDown(float delta ){

        if ( remainder > 0 )
            remainder = remainder - delta * 1.5f;

        if ( remainder <= 0 ){
            remainder = 0;
        }

        return remainder;
    }

    public boolean isFinished(){
        return remainder == 0;
    }


}
