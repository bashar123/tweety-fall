package com.snowdev.tweetyfall.models.count_down_screen;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Array;
import com.snowdev.tweetyfall.TweetyFallGame;
import com.snowdev.tweetyfall.models.loading_screen.AssetsHandler;
import com.snowdev.tweetyfall.models.shared.extensions.MyImage;
import java.util.Locale;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;
import static com.snowdev.tweetyfall.models.loading_screen.AssetsHandler.retrieveFont;
import static com.snowdev.tweetyfall.shared.Constants.CAMERA_HEIGHT;
import static com.snowdev.tweetyfall.shared.Constants.CAMERA_WIDTH;
import static com.snowdev.tweetyfall.shared.Constants.GAME_COUNTDOWN_DELAY;
import static com.snowdev.tweetyfall.shared.Constants.PositionXE.CENTER_X;
import static com.snowdev.tweetyfall.shared.Constants.PositionYE.CENTER_Y;
import static com.snowdev.tweetyfall.shared.Constants.PositionYE.VALUE_Y;
import static com.snowdev.tweetyfall.shared.Constants.ScreenE.GAME_SCREEN;

public class CountDownModel {

    private Counter counter;
    private Group countDownGroup;
    private TweetyFallGame game;
    private MyImage background, mountains, blurredBackground, blurredMountains;
    private boolean clearScreenFlag, countDownLabelAnimationEndedFlag;

    public CountDownModel( TweetyFallGame game) {
        this.game = game;

        counter = new Counter(GAME_COUNTDOWN_DELAY);

        background          = AssetsHandler.retrieveImage(AssetsHandler.GAME_BACKGROUND, false, CENTER_X, VALUE_Y, 0, 0);
        mountains           = AssetsHandler.retrieveImage(AssetsHandler.GAME_MOUNTAINS, false, CENTER_X, VALUE_Y, 0, 0);
        blurredBackground   = AssetsHandler.retrieveImage(AssetsHandler.MENU_BACKGROUND, false, CENTER_X, VALUE_Y, 0, 0);
        blurredMountains    = AssetsHandler.retrieveImage(AssetsHandler.MENU_MOUNTAINS, false, CENTER_X, CENTER_Y, 0, 0);

        initCountDown();
        showLabel();
        clarifyScreen();
    }

    private void clarifyScreen() {
        blurredBackground.addAction(
                fadeOut(2)
        );
        blurredMountains.addAction(
                sequence(
                        fadeOut(2),
                        run(new Runnable() {
                            @Override
                            public void run() {
                                clearScreenFlag = true;
                            }
                        })
                )
        );
    }


    private void initCountDown() {
        Label countDownLabel = new Label("3.00", new Label.LabelStyle(retrieveFont(), Color.WHITE));

        countDownGroup = new Group(); //To make scale-animation possible, cause its not possible to scale a label or font
        countDownGroup.setBounds(CAMERA_WIDTH/2 - countDownLabel.getWidth()/2, CAMERA_HEIGHT/2 - countDownLabel.getHeight()/2 + 150, countDownLabel.getWidth(), countDownLabel.getHeight());
        countDownGroup.setOrigin(countDownGroup.getWidth()/2,countDownGroup.getHeight()/2);
        countDownGroup.addActor( countDownLabel);

        countDownGroup.addAction(scaleTo(0,0)); // hide the counter to scale it later
    }


    public Array<Actor> getScreenImages(){
        Array<Actor> allMenuUiActors;
        allMenuUiActors = new Array<>();

        allMenuUiActors.addAll(
                background,
                mountains,
                blurredBackground,
                blurredMountains,
                countDownGroup
        );

        return allMenuUiActors;
    }

    public void update(float delta) {
        float count = counter.countDown(delta);
        updateCountDownLabel(count);

        if (clearScreenFlag) {
            // animate  moon, ground, nest
            System.out.println("clear screen flag");
        }

        if(counter.isFinished()){
            hideLabel();
        }

        if(countDownLabelAnimationEndedFlag){
            game.setCurrentScreen(GAME_SCREEN);
        }

    }

    private void updateCountDownLabel(float count ){
        ((Label) countDownGroup.getChildren().first()).setText(String.format(Locale.US,"%.2f", count));
    }

    private void showLabel(){
        countDownGroup.addAction(
                Actions.sequence(
                        Actions.scaleTo(0.08f,1, 0.1f), //todo add nice interpolation?
                        Actions.scaleTo(1,1, 0.1f) //todo add nice interpolation?
                )
        );
    }

    private void hideLabel(){
        if (!countDownGroup.hasActions()){
            countDownGroup.addAction(
                    Actions.sequence(
                            Actions.scaleTo(0.05f,1, 0.1f), //todo add nice interpolation?
                            Actions.scaleTo(0.05f,0, 0.1f), //todo add nice interpolation?
                            run(
                                    new Runnable() {
                                        @Override
                                        public void run() {
                                            countDownLabelAnimationEndedFlag = true;
                                        }
                                    }
                            )
                    )
            );
        }
    }


}
