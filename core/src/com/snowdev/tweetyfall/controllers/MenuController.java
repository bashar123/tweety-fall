package com.snowdev.tweetyfall.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.snowdev.tweetyfall.TweetyFallGame;
import com.snowdev.tweetyfall.shared.Constants;
import com.snowdev.tweetyfall.shared.Utils;
import static com.snowdev.tweetyfall.shared.Constants.SLOWMO;


public class MenuController extends InputAdapter {

    private TweetyFallGame game;

    public MenuController(TweetyFallGame game){
        this.game = game;
    }

    @Override
    public boolean keyDown(int keycode) {
        switch (keycode){
            case Input.Keys.ESCAPE:
                Gdx.app.exit();
                break;
            // '-'
            case Input.Keys.MINUS:
                SLOWMO = Math.max(0.1f, SLOWMO - 0.2f);
                System.out.println("SlowMo is: " + SLOWMO);
                break;
            // '+'
            case Input.Keys.EQUALS:
                SLOWMO = Math.min(1, SLOWMO + 0.2f);
                System.out.println("SlowMo is: " + SLOWMO);
                break;
            case Input.Keys.S:
                game.setCurrentScreen(Constants.ScreenE.SPLASH_SCREEN);
                break;
            case Input.Keys.M:
                game.setCurrentScreen(Constants.ScreenE.MENU_SCREEN);
                break;
            case Input.Keys.G:
                game.setCurrentScreen(Constants.ScreenE.GAME_SCREEN);
                break;
            case Input.Keys.Z:
                Utils.changeZoom(game.cam, 2);
                break;
            case Input.Keys.X:
                Utils.changeZoom(game.cam, 0.5f);
                break;
        }
        return true;
    }
}
