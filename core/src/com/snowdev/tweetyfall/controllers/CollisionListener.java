package com.snowdev.tweetyfall.controllers;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.snowdev.tweetyfall.models.shared.BirdBodyUserData;

import static com.snowdev.tweetyfall.shared.Constants.*;

/**
 * Detects when two box2d bodies collide
 */
public class CollisionListener implements ContactListener {

    public boolean collisionWithGround;
    public ColorE collidedBirdColor;

    public CollisionListener() { }


    @Override
    public void beginContact(Contact contact) {
        Fixture a = contact.getFixtureA();
        Fixture b = contact.getFixtureB();


        Fixture ground = null, bird = null;
        if(a.getBody().getUserData().equals("ground")){ //todo: test by Bits
            ground = a;
        }
        if(b.getBody().getUserData().equals("ground")){ //todo: test by bits
            ground = b;
        }
        if(a.getFilterData().categoryBits == BIT_BIRD){
            bird = a;
        }
        if(b.getFilterData().categoryBits == BIT_BIRD) {
            bird = b;
        }

        BirdBodyUserData birdUserData = (BirdBodyUserData) bird.getBody().getUserData();
        if(ground != null){
            onCollisionDetected(true, birdUserData.getColor() );
        }
        else if( a.getFilterData().categoryBits != BIT_BIRD || b.getFilterData().categoryBits != BIT_BIRD ){ //test so 2 birds wont collide with each other
            onCollisionDetected(false, birdUserData.getColor());
        }

    }

    private void onCollisionDetected(boolean withGround, ColorE birdColor){
        collisionWithGround = withGround;
        collidedBirdColor = birdColor;
    }



    @Override
    public void endContact(Contact contact) {}

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {}

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {}
}
