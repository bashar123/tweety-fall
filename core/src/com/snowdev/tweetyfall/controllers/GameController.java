package com.snowdev.tweetyfall.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Vector2;
import com.snowdev.tweetyfall.models.game_screen.GameFacade;
import com.snowdev.tweetyfall.shared.Constants;
import com.snowdev.tweetyfall.shared.Utils;

import static com.snowdev.tweetyfall.shared.Constants.SLOWMO;


public class GameController extends InputAdapter {

    private GameFacade facade;
    private int maxFingers = 0;

    public GameController(GameFacade modelFacade){
        this.facade = modelFacade;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        maxFingers = pointer;
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if(pointer == maxFingers){

            if(pointer == 0)
                facade.nextNestColor();

            if(pointer == 1)
                facade.startScreen(Constants.ScreenE.GAME_SCREEN);

            if(pointer == 2)
                facade.togglePaused();

            maxFingers = 0;
        }
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        facade.updatePos(new Vector2(screenX, screenY), true);
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        facade.updatePos(new Vector2(screenX, screenY), false);
        return true;
    }


    @Override
    public boolean keyDown(int keycode) {
        switch (keycode){
            case Input.Keys.ESCAPE:
                Gdx.app.exit();
                break;
            case Input.Keys.MINUS:
                SLOWMO = Math.max(0.1f, SLOWMO - 0.2f);
                System.out.println("SlowMo is: " + SLOWMO);
                break;
            case Input.Keys.EQUALS: // '+'
                SLOWMO = Math.min(1, SLOWMO + 0.2f);
                System.out.println("SlowMo is: " + SLOWMO);
                break;
            case Input.Keys.S:
                facade.startScreen(Constants.ScreenE.SPLASH_SCREEN);
                break;
            case Input.Keys.M:
                facade.startScreen(Constants.ScreenE.MENU_SCREEN);
                break;
            case Input.Keys.G:
                facade.startScreen(Constants.ScreenE.GAME_SCREEN);
                break;
            case Input.Keys.Z:
                facade.changeZoom(2);
                break;
            case Input.Keys.X:
                facade.changeZoom(0.5f);
                break;
            case Input.Keys.D:
                facade.toggleBox2dDebugBorders();
                break;
            default:
                facade.togglePaused();
                break;
        }
        return true;
    }


    public static Vector2 getAcceleration(){
        return new Vector2(Gdx.input.getAccelerometerX(), Gdx.input.getAccelerometerY());
    }
}
