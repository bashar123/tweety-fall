package com.snowdev.tweetyfall.controllers;

import com.badlogic.gdx.InputAdapter;
import com.snowdev.tweetyfall.TweetyFallGame;
import com.snowdev.tweetyfall.shared.Constants;
import com.snowdev.tweetyfall.shared.Utils;


public class LoadingController extends InputAdapter {

    private TweetyFallGame game;


    public LoadingController(TweetyFallGame game){
        this.game = game;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        Constants.ScreenE.SECOND_SCREEN = Constants.ScreenE.SPLASH_SCREEN;
        Utils.showToast(game.androidInterface, "Next screen is splash screen.");
        return true;
    }
}
