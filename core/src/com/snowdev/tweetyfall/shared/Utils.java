package com.snowdev.tweetyfall.shared;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.TimeUtils;
import com.snowdev.tweetyfall.android.AndroidInterface;


public class Utils {
    /**
     * convert pixel to meter for the Box2d world (uses meters)
     * @param pixel pixels to be converted to meter
     * @return meter - float
     */
    public static float PPM(int pixel){
        return pixel/ Constants.PPM;
    }

    /**
     * convert pixel to meter for the Box2d world (uses meters)
     * @param pixel pixels to be converted to meter
     * @return meter - float
     */
    public static float PPM(float pixel){
        return pixel/ Constants.PPM;
    }


    public static void setWakeLock(AndroidInterface androidInterface, boolean awake){
        if (!Constants.DESKTOP_VERSION) {
            if(awake)
                androidInterface.screenOn();
            else
                androidInterface.screenOff();
        }
    }

    public static void showToast(AndroidInterface androidInterface, String text)
    {
        if(!Constants.DESKTOP_VERSION)
            androidInterface.showToast(text);
        else
            System.out.println("Toast text: " + text);
    }

    public static void changeZoom(OrthographicCamera cam, float zoom){
        cam.viewportWidth = cam.viewportWidth * zoom;
        cam.viewportHeight = cam.viewportHeight * zoom;
        cam.update();
    }

    public static long getCurrentTimeInMillis(){
        long time = TimeUtils.nanoTime() ;
        return TimeUtils.nanosToMillis(time);
    }



}