package com.snowdev.tweetyfall.shared;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;

public class Constants {
    //Development flags
    public static  final boolean ShowScreenSize             = true;
    public static final boolean DISABLE_GAMEOVER            = true;
    public static boolean DEBUG_RENDER                      = false;
    public static final boolean BOX2D_LIGHTS                = false;
    public static final boolean DISABLE_SOUNDS              = false; // TODO implement
    public static boolean DESKTOP_VERSION;                   //dynamically stored once when game is initialized

    public static final String GAME_TITLE                   = "Tweety Fall";
    public static final String THEME                        = "Original"; //"Material" //"Fishes" med bubblor (akwarium) - ToDo
    public static final String GOOGLE_PLAY_URI              = "https://play.google.com/store/apps/details?id=com.shagunstudios.racinggame";

    //World / Box2d
    public static final float PPM                           = 500.0f;
    public static final float TIME_STEP                     = 1.0f / PPM;
    public static final float WORLD_GRAVITY                 = -1.0f;
    public static final float MENU_HORIZONTAL_ACC_SPEED     = 0.5f;
    public static final byte BIT_BIRD                       = 4;
    public static final byte BIT_GROUND                     = 4;
    public static final byte BIT_CLOUD                      = 8;


    //Graphics
    public static final float CAMERA_WIDTH                  = 1080;
    public static final float CAMERA_HEIGHT                 = 1920;
    public static final float CAMERA_WIDTH_METER            = CAMERA_WIDTH / PPM;
    public static final float CAMERA_HEIGHT_METER           = CAMERA_HEIGHT / PPM;

    public static final byte  NUMBER_OF_STARS               = 60;

    // Desktop
    public static final byte LWJGL_APPLICATION_SCALE        = 4; //Desktop window size-scale
    public static final int LWJGL_APPLICATION_WIDTH         = 1440 / LWJGL_APPLICATION_SCALE;
    public static final int LWJGL_APPLICATION_HEIGHT        = 2960 / LWJGL_APPLICATION_SCALE;
    public static final boolean HIDE_LWJGL_WINDOW_BORDER    = false;

    //Time
    public static final float POWERUP_DURATION                  = 5000; //ms
    public static final float WAIT_TIME_TO_CREATE_NEW_BIRD_MENU = 0.11f;  //s
    public static final float WAIT_TIME_TO_CREATE_NEW_BIRD_GAME = 0.5f;  //s
    public static final float BIRD_FALLING_SPEED                = 1f;  //s //TODO world gravity?

    //Speeds
    public static float SLOWMO                              = 1f;
    public static final float FILTER_ALPHA                  = 0.7f;
    public static final float NEST_ACCELERATION_SPEED       = 6 / PPM;
    public static final float UPPER_CLOUD_SPEED             = 200 / PPM;
    public static final float LOWER_CLOUD_SPEED             = 100 / PPM;
    public static final float BIRD_ANIMATION_SPEED          = 1 / 30f;
    public static final float FEATHER_ANIMATION_SPEED       = 1 / 35f;
    public static final float NEST_ANIMATION_SPEED          = 1 / 100f;

    public static final float GAMESCREEN_SLIDE_SPEED        = 1.8f;
    public static final float LOADING_ANIMATION_SPEED       = 0.8f;
    public static final float STAR_ANIMATION_DELAY          = 0.05f;
    public static final float GAME_COUNTDOWN_DELAY          = 3f;

    //Image sizes
    //todo


    //Game logic
    public static final byte NR_OF_COLUMNS                  = 7; //birds columns
    /** Total of {@value BIRD_IMAGE_POOL_SIZE} * 2; {@value BIRD_IMAGE_POOL_SIZE} redbirds and {@value BIRD_IMAGE_POOL_SIZE} bluebirds */
    public static final int BIRD_IMAGE_POOL_SIZE            = 100;


    //Colors
    public static final Color LOADING_BAR_COLOR             = new Color(0xe47f37ff);
    public static final Color SCORE_COLOR                   = new Color(0xff9d3eff);


    //Interpolations
    public static final Interpolation LOADING_ANIMATION_INTERP  = Interpolation.exp10;
    public static final Interpolation GAME_ANIMATION_INTERP     = Interpolation.exp10Out; //ground slide up, moon slide, cloud slide down, stars fading,

    public enum ColorE {
        BLUE_BIRD, RED_BIRD,
        BLUE_NEST, BLUE_NEST_ANIMATING, RED_NEST, RED_NEST_ANIMATING
    }


    public enum MatchStateE {
        BLUE_BIRD_MATCH, BLUE_BIRD_MISMATCH, RED_BIRD_MATCH, RED_BIRD_MISMATCH
    }


    public enum PowerUpE {
        NO_POWERUP, WIDE_NEST_POWERUP, BIG_BIRD_POWERUP, MAGNET_POWERUP, DOUBLE_POINT_POWERUP;
    }


    public enum ScreenE {
        LOADING_SCREEN, SPLASH_SCREEN, MENU_SCREEN, GAME_SCREEN, COUNT_DOWN_SCREEN, GAMEOVER_SCREEN, TESTING_SCREEN, TESTING_SCREEN2;
        public static ScreenE SECOND_SCREEN = MENU_SCREEN; //After LOADING_SCREEN
    }


    public enum LevelE{
        LEVEL1(25), LEVEL2(50), LEVEL3(100), LEVEL4(200), LEVEL5(400);

        public int points;
        LevelE(int flag){this.points = flag;}
    }

    public enum PositionXE {
        CENTER_X, LEFT, RIGHT, VALUE_X
    }

    public enum PositionYE {
        CENTER_Y, TOP , BOTTOM, VALUE_Y
    }
}
