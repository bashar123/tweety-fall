package com.snowdev.tweetyfall.toBeRefactored;

import static com.snowdev.tweetyfall.shared.Constants.*;
import static com.snowdev.tweetyfall.shared.Constants.PowerUpE.*;


public class PowerUpModel {
    private int progress;
    private PowerUpE currentPowerUp ;
    private float powerUpStartTime;

    public PowerUpModel(){
        progress = 0;
        currentPowerUp = NO_POWERUP;

    }


    public void update(){

        switch (currentPowerUp) {
            case NO_POWERUP:
                incrementProgress();
                if(progress==100)
                    runWideNestPowerUp();
                break;

            case WIDE_NEST_POWERUP:
                checIfTimesUp(WIDE_NEST_POWERUP);
                break;
        }
    }

    public void checIfTimesUp(PowerUpE powerUp){
        //get current time
        //if(currenttime - powerUpStartTime >= POWERUP_DURATION){
        //      if satser för att -> endWideNestPowerUp
        //
        //powerUpStartTime = 0
        //clearProgress();
        //currentPowerUp = NO_POWERUP;
    }

    public PowerUpE getCurrentPowerUp() {
        return currentPowerUp;
    }

    public void setCurrentPowerUp(PowerUpE currentPowerUp) {
        this.currentPowerUp = currentPowerUp;
    }

    public void incrementProgress(){
        progress += 5;
    }

    public void clearProgress(){
        progress = 0;
    }

    public void runWideNestPowerUp(){
        //currentPowerUp = WIDE_NEST_POWERUP;
        //make nest body wider
        //animate the sprite
        //get current time -> powerUpStartTime
    }

    public void endWideNestPowerUp(){
        //make nest body smaller
        //animate the sprite
    }

    public void runMagnetNestPoweUp(){
//        for(Body bird: birdBodies) {
//            if (bird.getPosition().y < CAMERA_HEIGHT_METER) {
//                bird.setLinearVelocity( (nestBodies.get(1).getPosition().x - bird.getPosition().x) * 0.4f, bird.getLinearVelocity().y);
//            }
//        }
    }

//    public void magnet(){
//        float dx,dy,diff,cloudWidth;
//        Vector2 birdPos=new Vector2(), cloudPos = new Vector2();
//
//        for(Body bird: birdBodies) {
//            birdPos.set(bird.getPosition().x,bird.getPosition().y);
//            cloudPos.set(nestBodies.get(1).getPosition().x,nestBodies.get(1).getPosition().y);
//            cloudWidth = ((Rectangle)nestBodies.get(1).getUserData()).getWidth();
////            diff = 1;
////            dx = cloudPos.x - birdPos.x;
////            dy = cloudPos.y - birdPos.y;
////
////            if(dx<dy){
////                diff = dx/dy;
////            }
//
//
//            if (birdPos.y < CAMERA_HEIGHT_METER/1.3f && (birdPos.x>cloudPos.x - PPM(400) && birdPos.x< cloudPos.x + PPM(400))) {
//
//
//                if (birdPos.x < cloudPos.x) {
//                    if (bird.getLinearVelocity().x == 0) {
//                        bird.setLinearVelocity(1f, bird.getLinearVelocity().y);
//                    }
//
//                    bird.applyForceToCenter(bird.getLinearVelocity().x * 3.5f, bird.getLinearVelocity().y * 2f, false);
//                } else if (birdPos.x <= cloudPos.x + ((Rectangle) nestBodies.get(1).getUserData()).getWidth()) {
//                    bird.setLinearVelocity(0, bird.getLinearVelocity().y);
//                } else if (birdPos.x > cloudPos.x + ((Rectangle) nestBodies.get(1).getUserData()).getWidth()) {
//                    if (bird.getLinearVelocity().x == 0) {
//                        bird.setLinearVelocity(-1f, bird.getLinearVelocity().y);
//                    }
//
////                    bird.setLinearVelocity(-(3.6f - (birdPos.x - cloudPos.x)) * 1f , bird.getLinearVelocity().y);
//                    bird.applyForceToCenter(bird.getLinearVelocity().x * 3.5f, bird.getLinearVelocity().y * 2f, false);
//                }
//
//            }
//
//        }
//    }

}
