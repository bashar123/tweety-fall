package com.snowdev.tweetyfall.toBeRefactored;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.snowdev.tweetyfall.shared.Utils;


import static com.snowdev.tweetyfall.shared.Constants.*;
import static com.snowdev.tweetyfall.shared.Constants.LevelE.*;


public class LevelModel {
	private final Array<Float> xPositions;
	private Array<Image> colorIconImages;
	private int level, oldScore;
	private ColorE oldNestColor;


	//TODO: Green = 99CC99?? Purple color: 996699??
	public LevelModel(Array<Image> colorIconImages, ColorE initialNestColor ){

		this.colorIconImages = colorIconImages;
		this.oldNestColor = initialNestColor;
		this.oldScore = 0;
		this.level = 1;

		xPositions = new Array<>();
		xPositions.addAll(Utils.PPM(100), Utils.PPM(200), 0f, 0f, 0f);

		updateImagesPosition();
	}

	private void updateImagesPosition(){
		for(int i = 0; i< colorIconImages.size; i++ ){
			colorIconImages.get(i).setPosition(xPositions.get(i), CAMERA_HEIGHT_METER/2);
		}
	}


	public void update(ColorE currentNestColor, int score){
		if(currentNestColor!=oldNestColor){
			nextColorState();
		}
		if(score!=oldScore){
			checkIfNewLevel(score);
		}

	}

	private void nextColorState() {
		//move the first image to end
		colorIconImages.add(colorIconImages.removeIndex(0));
		//updatePositions
		updateImagesPosition();
	}


	private void checkIfNewLevel(int score){
		if(score >= LEVEL4.points){
			createNewColorState(6);
		}

		else if(score >= LEVEL3.points){
			createNewColorState(5);
		}

		else if(score >= LEVEL2.points){
			createNewColorState(4);
		}

		else if(score >= LEVEL1.points){
			createNewColorState(3);
		}

	}

	private void createNewColorState(int numberOfColorIcons){

	}








}
