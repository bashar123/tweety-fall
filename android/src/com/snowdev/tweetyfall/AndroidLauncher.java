package com.snowdev.tweetyfall;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;


public class AndroidLauncher extends AndroidApplication {

    private final int
            MULTIPLE_PERMISSIONS_REQUEST_CODE = 1;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        getRequiredPermissions();

        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useCompass = false;
		config.useAccelerometer = true;
        config.useImmersiveMode = true;

//        config.r = 8;
//        config.g = 8;
//        config.b = 8;
//        config.a = 8;
        initialize(new TweetyFallGame(new AndroidInterfaceClass(this)), config);
    }

    private void getRequiredPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                this.requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, MULTIPLE_PERMISSIONS_REQUEST_CODE);
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0 || grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED )
                    finish();
                break;
            }
        }
    }

//  fungerande
//	@Override
//	public void onWindowFocusChanged(boolean hasFocus) {
//		super.onWindowFocusChanged(hasFocus);
//		if (hasFocus && Build.VERSION.SDK_INT >= 19 ) {
//			getWindow().getDecorView().setSystemUiVisibility(
//					View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
//							View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
//							View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
//							View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
//							View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
//		}
//	}

//	@Override
//	public void onWindowFocusChanged(boolean hasFocus) {
//		super.onWindowFocusChanged(hasFocus);
//		if (hasFocus && Build.VERSION.SDK_INT >= 19) {
//			getWindow().getDecorView().setSystemUiVisibility(
//					View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//							| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//							| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//							| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//							| View.SYSTEM_UI_FLAG_FULLSCREEN
//							| View.SYSTEM_UI_FLAG_IMMERSIVE);
//		}
//	}

//	private void hideSystemUi() {
//		if (Build.VERSION.SDK_INT >= 19) {
//			getWindow().getDecorView().setSystemUiVisibility(
//					View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//							| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//							| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//							| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//							| View.SYSTEM_UI_FLAG_FULLSCREEN
//							| View.SYSTEM_UI_FLAG_IMMERSIVE);
//		}
//	}
}
