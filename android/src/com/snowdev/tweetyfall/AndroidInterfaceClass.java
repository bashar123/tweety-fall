package com.snowdev.tweetyfall;

import android.content.Context;
import android.provider.Settings;
import android.view.WindowManager;
import android.widget.Toast;
import com.snowdev.tweetyfall.android.AndroidInterface;


public class AndroidInterfaceClass implements AndroidInterface {
    private AndroidLauncher launcher;

    public AndroidInterfaceClass(AndroidLauncher launcher){
        this.launcher = launcher;
    }

    @Override
    public void screenOn() {
        launcher.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                launcher.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                Toast toast=Toast.makeText(launcher.getContext(), "Screen will stay awake", Toast.LENGTH_SHORT);
                toast.show();

            }
        });
    }

    @Override
    public void screenOff() {
        launcher.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                launcher.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                Toast toast= Toast.makeText(launcher.getContext(), "Screen will NOT stay awake", Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }

    @Override
    public void showToast(final String text) {
        launcher.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast toast= Toast.makeText(launcher.getContext(), text, Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }


}
