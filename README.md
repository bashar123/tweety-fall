# README #

## **Tweety Fall** ##

Clone or download the project and import to AndroidStudio 
Create a desktop run configuration: working directory = tweety-fall\android\assets
For more information see libgdx documentaion: https://libgdx.badlogicgames.com/documentation/gettingstarted/Running%20and%20Debugging.html


### About ###

An Android/desktop simplistic game, written in Java with the **LibGDX** Java game development framework and **Box2D**, a physics engine.
Everything about the game (fysics, animations, ui etc..) is made by coding, no external engines were involved in the development.

Q:Why no use a game engine?
A:The premis of this project is to learn more about gamne development, Android and improve my coding skills (eg by impelementing some design patterns)

Version: (not yet released/ pre-alpha)